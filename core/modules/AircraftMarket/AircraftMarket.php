<?php
class AircraftMarket extends CodonModule {
    public function index(){
	        $this->completemaintenance();
            $this->payleasingrate();
            AircraftMarketData::disablesoldandleased();
            AircraftMarketData::deletefinishedlease();
            $settings = AircraftMarketData::getsettings();
            if($settings->disablecrashed == '1') {
                AircraftMarketData::disableallcrashed();
            }
            $query0 = "SELECT * FROM ".TABLE_PREFIX."pireps WHERE accepted = 1 ORDER BY submitdate ASC";

            $pireps = DB::get_results($query0);

            foreach($pireps as $pirep) {
                echo $pirep->pirepid;
                $query1 = "SELECT * FROM aircraftmarket_damagehistory WHERE pirepid = '$pirep->pirepid'";

                $rodemo = DB::query($query1);

                if($rodemo > 0){
                    continue;
                }

                $aircraft = $pirep->aircraft;
                $landingrate = $pirep->landingrate;
                $flighttime = $pirep->flighttime;
                $plog = $pirep->log;
                $pirepid = $pirep->pirepid;

                if (strpos($plog, 'stall') !== false || strpos($plog, 'Stall') !== false) {
                    $stallflag = '1';
                } else {
                    $stallflag = 'O';
                }
                if (strpos($plog,'overspeed') !== false || strpos($plog,'Overspeed') !== false) {
                    $speedflag = '1';
                } else {
                    $speedflag = 'O';
                }

                $aircraftflights = AircraftMarketData::getaircraftpirepdata($aircraft);
                AircraftMarketData::updateaircraftflightdata($aircraft, $aircraftflights->totalhours, $aircraftflights->totalflights, $pirep->arricao);
                $this->processaircraftstate($aircraft, $landingrate, $flighttime, $stallflag, $speedflag, $pirepid);
                $this->sendtomaintenance($aircraft);
            }

            return;
	}

    public function __construct() {
        CodonEvent::addListener('AircraftMarket', array('pirep_filed'));
    }

    public function EventListener($eventinfo) {
        $eventname = $eventinfo[0]; // Event name
        $eventmodule = $eventinfo[1]; // Class calling it
        if($eventinfo[0] == 'pirep_filed'){ 
            self::completemaintenance();
            self::payleasingrate();
            AircraftMarketData::disablesoldandleased();
            AircraftMarketData::deletefinishedlease();
            $settings = AircraftMarketData::getsettings();
            if($settings->disablecrashed == '1') {
                AircraftMarketData::disableallcrashed();
            }
            $query0 = "SELECT * FROM ".TABLE_PREFIX."pireps ORDER BY submitdate DESC LIMIT 1";
					
			$pirep =	DB::get_row($query0);

            $aircraft = $pirep->aircraft;
            $landingrate = $pirep->landingrate;
            $flighttime = $pirep->flighttime;
            $plog = $pirep->log;
            $pirepid = $pirep->pirepid;

            if (strpos($plog, 'stall') !== false || strpos($plog, 'Stall') !== false) {
                $stallflag = '1';
            } else {
                $stallflag = 'O';
            }
            if (strpos($plog,'overspeed') !== false || strpos($plog,'Overspeed') !== false) {
                $speedflag = '1';
            } else {
                $speedflag = 'O';
            }

            $aircraftflights = AircraftMarketData::getaircraftpirepdata($aircraft);
            AircraftMarketData::updateaircraftflightdata($aircraft, $aircraftflights->totalhours, $aircraftflights->totalflights, $pirep->arricao);
            self::processaircraftstate($aircraft, $landingrate, $flighttime, $stallflag, $speedflag, $pirepid);
            self::sendtomaintenance($aircraft);
            return;
        }
    }

    public function processaircraftstate($aircraft, $landingrate, $flighttime, $stallflag, $speedflag, $pirepid) {
        if(!$aircraft){
            return;
        }

        if($landingrate < '-2000') {
            $settings = AircraftMarketData::getsettings();
            if($settings->disablecrashed=='1') {
                AircraftMarketData::aircraftcrash($aircraft);
                AircraftMarketData::disableaircraft($aircraft);
                AircraftMarketData::disableschedules($aircraft);
                if($settings->switchaircraft = "1") {
                    $aircraftdata = AircraftMarketData::getAircraftData($aircraft);
                    self::switchscheduleaircraft($aircraft, $aircraftdata->icao);
                }
            }
            return;
        }

        if($landingrate < '-1000') {
            $structuredamage = mt_rand(1,4).'.'.mt_rand(1,7);
            $geardamage = mt_rand(2,5).'.'.mt_rand(1,7);
        } elseif($landingrate < '-600') {
            $structuredamage = '0.'.mt_rand(1,4);
            $geardamage = mt_rand(1,3).'.'.mt_rand(1,6);
        } elseif($landingrate < '-300') {
            $structuredamage = '0.'.mt_rand(1,2);
            $geardamage = '0.'.mt_rand(1,3);
        } else {
            $structuredamage = '0.'.mt_rand(1,2);
            $geardamage = '0.'.mt_rand(1,2);
        }

        if($flighttime > 10) {
            $enginedamage = '0.'.mt_rand(2,6);
            $structuredamage = $structuredamage + '0.'.mt_rand(1,3);
        } elseif($flighttime > 5) {
            $enginedamage = '0.'.mt_rand(1,3);
            $structuredamage = $structuredamage + '0.'.mt_rand(1,2);
        } else {
            $enginedamage = '0.'.mt_rand(1,2);
            $structuredamage = $structuredamage + '0.'.mt_rand(1,2);
        }

        if($stallflag == '1') {
            $structuredamage = $structuredamage + '1.'.mt_rand(1,5);
        } if($speedflag == '1') {
            $structuredamage = $structuredamage + mt_rand(1,3).'.'.mt_rand(1,6);
        }

        $flightdamage = $enginedamage + $structuredamage + $geardamage;

        $aircraftdata = AircraftMarketData::getAircraftData($aircraft);

        if(!$aircraftdata->totalstate) {
            $oldoverall = '100.0';
        } else {
            $oldoverall = $aircraftdata->totalstate;
        }

        if(!$aircraftdata->enginestate) {
            $oldengine = '100.0';
        } else {
            $oldengine = $aircraftdata->enginestate;
        }

        if(!$aircraftdata->structurestate) {
            $oldstructure = '100.0';
        } else {
            $oldstructure = $aircraftdata->structurestate;
        }

        if(!$aircraftdata->gearstate) {
            $oldgear = '100.0';
        } else {
            $oldgear = $aircraftdata->gearstate;
        }

        $newoverall = $oldoverall - $flightdamage;
        $newengine = $oldengine - $enginedamage;
        $newstructure = $oldstructure - $structuredamage;
        $newgear = $oldgear - $geardamage;

        AircraftMarketData::storedamagehistory($aircraft, $pirepid, $enginedamage, $structuredamage, $geardamage, $flightdamage, $newoverall, $newengine, $newstructure, $newgear, $oldoverall, $oldengine, $oldstructure, $oldgear);

        AircraftMarketData::updateaircraftstate($aircraft, $newoverall, $newengine, $newstructure, $newgear);
    }

    public function sendtomaintenance($aircraft) {
        $settings = AircraftMarketData::getsettings();
        $lastcheck = AircraftMarketData::getaircraftlastcheckhours($aircraft);
        $lastChours = AircraftMarketData::getaircraftlastCcheckhours($aircraft);
        $lastILhours = AircraftMarketData::getaircraftlastILcheckhours($aircraft);
        $lastDhours = AircraftMarketData::getaircraftlastDcheckhours($aircraft);
        $maintcompany = AircraftMarketData::getmaintcompdata($settings->maintcomp);
        $aircraftdata = AircraftMarketData::getAircraftData($aircraft);
        $aircrafthours = round($aircraftdata->totalhours);
        $overallstate = round($aircraftdata->totalstate);

        if($aircraftdata->totalstate < $settings->repairstate) {
            //Repair Aircraft if it falls below a certain state
            $repspeed = $maintcompany->repspeed;
            $repprice = $maintcompany->repprice;
            $torepair = 100 - $overallstate;
            
            if($torepair <= 5) {
                $torepair = 7;
            }
            $repairdays = round(($torepair / $repspeed) / 1.5);
            $price = round($torepair * $repprice);

            $today = date('Y-m-d H:i:s');
                if($repairdays < 1) {
                    $until = date('Y-m-d H:i:s', strtotime($today. ' + 12 hour'));
                } else {
                    $until = date('Y-m-d H:i:s', strtotime($today. ' + '.$repairdays.' day'));
                }

            $checktype = 'Damage';
            AircraftMarketData::addmaintenancehistory($aircraft, $aircrafthours, $checktype, $today, $until, $repairdays, $price, $overallstate);
            AircraftMarketData::disableaircraft($aircraft);
            AircraftMarketData::disableschedules($aircraft);

            //Move Schedules to other aircraft of same type if option enabled
            if($settings->switchaircraft = "1"){
                self::switchscheduleaircraft($aircraft, $aircraftdata->icao);
            }


            $expensedate = date('Ym');
            $expensetype = 'M';
            $expensename = 'Maintenance Cost ('.$aircraftdata->registration.')';
            AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $price);

        } elseif(($aircrafthours - $lastDhours) > '5000' && (!$lastcheck || $lastcheck > '50')) {
            //Send Aircraft to D Check (~21 days out of service)
            $comdays = $maintcompany->checkdaysD;
            $today = date('Y-m-d H:i:s');
            $until = date('Y-m-d H:i:s', strtotime($today. ' + '.$comdays.' day'));
            $price = $maintcompany->servicepriceD;
            $checktype = 'D';
            AircraftMarketData::addmaintenancehistory($aircraft, $aircrafthours, $checktype, $today, $until, $comdays, $price, $overallstate);
            AircraftMarketData::disableaircraft($aircraft);
            AircraftMarketData::disableschedules($aircraft);

            //Move Schedules to other aircraft of same type if option enabled
            if($settings->switchaircraft = "1") {
                self::switchscheduleaircraft($aircraft, $aircraftdata->icao);
            }

            $expensedate = date('Ym');
            $expensetype = 'M';
            $expensename = 'D Check ('.$aircraftdata->registration.')';
            AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $price);
        } elseif(($aircrafthours - $lastILhours) > '3000' && (!$lastcheck || $lastcheck > '50')) {
            //Send Aircraft to IL Check (~14 days out of service)
            $comdays = $maintcompany->checkdaysIL;
            $today = date('Y-m-d H:i:s');
            $until = date('Y-m-d H:i:s', strtotime($today. ' + '.$comdays.' day'));
            $price = $maintcompany->servicepriceIL;
            $checktype = 'IL';
            AircraftMarketData::addmaintenancehistory($aircraft, $aircrafthours, $checktype, $today, $until, $comdays, $price, $overallstate);
            AircraftMarketData::disableaircraft($aircraft);
            AircraftMarketData::disableschedules($aircraft);

            //Move Schedules to other aircraft of same type if option enabled
            if($settings->switchaircraft = "1") {
                self::switchscheduleaircraft($aircraft, $aircraftdata->icao);
            }

            $expensedate = date('Ym');
            $expensetype = 'M';
            $expensename = 'IL Check ('.$aircraftdata->registration.')';
            AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $price);
        } elseif(($aircrafthours - $lastChours) > '1000' && (!$lastcheck || $lastcheck > '50')) {
            //Send Aircraft to C Check (~7 days out of service)
            $comdays = $maintcompany->checkdaysC;
            $today = date('Y-m-d H:i:s');
            $until = date('Y-m-d H:i:s', strtotime($today. ' + '.$comdays.' day'));
            $price = $maintcompany->servicepriceC;
            $checktype = 'C';
            AircraftMarketData::addmaintenancehistory($aircraft, $aircrafthours, $checktype, $today, $until, $comdays, $price, $overallstate);
            AircraftMarketData::disableaircraft($aircraft);
            AircraftMarketData::disableschedules($aircraft);

            //Move Schedules to other aircraft of same type if option enabled
            if($settings->switchaircraft = "1")
            {
            self::switchscheduleaircraft($aircraft, $aircraftdata->icao);
            }

            $expensedate = date('Ym');
            $expensetype = 'M';
            $expensename = 'C Check ('.$aircraftdata->registration.')';
            AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $price);
        } else{
            return;
        }
    }


    public function completemaintenance() {
        $aircrafttoactivate = AircraftMarketData::getaircrafttocompletemaint();
        if($aircrafttoactivate)
        foreach($aircrafttoactivate as $aircraft) {
            AircraftMarketData::updateaircraftstate($aircraft->aircraftid, '100', '100', '100', '100');
            AircraftMarketData::enableaircraft($aircraft->aircraftid);
            AircraftMarketData::enableschedules($aircraft->aircraftid);
        }
    }

    public function switchscheduleaircraft($oldid, $icao) {
        $newac = AircraftMarketData::searchavailableaircraft($icao);

        if($newac) {
            AircraftMarketData::switchacschedules($newac->id, $oldid);
            AircraftMarketData::enableaircraft($newac->id);
            AircraftMarketData::enableschedules($newac->id);
        }
    }

    public function payleasingrate() {
        $leasetopay = AircraftMarketData::getleasetopay();

        if($leasetopay)
            foreach($leasetopay as $lease) {
                if($lease->remainingvalue > '0'){
                    $expensevalue = $lease->leasingrate;
                    $expensename = 'Aircraft Leasing ('.$lease->acregistration.')';
                    $expensedate = date('Ym');
                    $expensetype = 'M';

                    $newvalue = $lease->remainingvalue - $lease->leasingrate;
                    AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $expensevalue);
                    AircraftMarketData::updateleasing($lease->id, $newvalue);
                }
            }
    }
}
?>