<?php
/** Edited at 11/11/2020 by Arthur Hetem
 * 
 */
class AircraftMarketData extends CodonModule {

	public static function getsettings() {
		$sql="SELECT * FROM aircraftmarket_settings";
		return DB::get_row($sql);
	}

	public static function savesettings($repairstate, $disablecrashed, $switchaircraft) {
 		$query = "UPDATE aircraftmarket_settings SET repairstate='$repairstate', disablecrashed='$disablecrashed', switchaircraft = '$switchaircraft' WHERE id='1'";
		
        DB::query($query);
	}

	public static function savecontractor($contractor) {
 		$query = "UPDATE aircraftmarket_settings SET maintcomp='$contractor' WHERE id='1'";
		
        DB::query($query);
	}

	public static function getaircraftpirepdata($aircraft) {
		$sql="SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(flighttime_stamp))) as totalhours, COUNT(aircraft) as totalflights FROM ".TABLE_PREFIX."pireps WHERE aircraft = '$aircraft'";
		
		return DB::get_row($sql);
	}

	public static function updateaircraftflightdata($aircraft, $totalhours, $totalflights, $location) {
		$checkac = self::checkaircraftinlist($aircraft);

		if(!$checkac) {
			$query1 = "INSERT INTO aircraftmarket_aircraftlist (aircraftid, totalhours, totalflights, location)
									VALUES('$aircraft', '$totalhours', '$totalflights', '$location')";
								
			DB::query($query1);
		} else {
 			$query = "UPDATE aircraftmarket_aircraftlist SET totalhours='$totalhours', totalflights='$totalflights', location='$location' WHERE aircraftid='$aircraft'";
		
        	DB::query($query);
		}
	}

	public static function aircraftcrash($aircraft) {
		$query = "UPDATE aircraftmarket_aircraftlist SET crashed='1', gearstate = '0', totalstate ='0', enginestate = '0' structurestate = '0' WHERE aircraftid='$aircraft'";
		
		DB::query($query);
	}

	public static function disableaircraft($aircraft) {
		$query = "UPDATE ".TABLE_PREFIX."aircraft SET enabled='0' WHERE id='$aircraft'";
			
		DB::query($query);
	}

	public static function disableschedules($aircraft) {
		$query = "UPDATE ".TABLE_PREFIX."schedules SET enabled='0' WHERE aircraft='$aircraft'";
		
		DB::query($query);
	}

	public static function enableaircraft($aircraft) {
		$query = "UPDATE ".TABLE_PREFIX."aircraft SET enabled='1' WHERE id='$aircraft'";
			
		DB::query($query);
	}

	public static function enableschedules($aircraft) {
		$query = "UPDATE ".TABLE_PREFIX."schedules SET enabled='1' WHERE aircraft='$aircraft'";
			
		DB::query($query);
	}

	public static function getAircraftData($aircraft) {
		$sql="SELECT a.*, l.* FROM ".TABLE_PREFIX."aircraft a
			LEFT JOIN aircraftmarket_aircraftlist l ON a.id = l.aircraftid WHERE a.id = '$aircraft'";

		return DB::get_row($sql);
	}

	public static function checkaircraftinlist($aircraft) {
		$sql="SELECT * FROM aircraftmarket_aircraftlist WHERE aircraftid = '$aircraft'";
			
		return DB::get_row($sql);
	}

	public static function storedamagehistory($aircraft, $pirepid, 
		$enginedamage, $structuredamage, $geardamage, $flightdamage, 
		$newoverall, $newengine, $newstructure, $newgear, $oldoverall, 
		$oldengine, $oldstructure, $oldgear) {
			$query = "INSERT INTO aircraftmarket_damagehistory (aircraftid, pirepid, enginedamage, structuredamage, geardamage, totaldamage, newstate, newengine, newstructure, newgear, oldstate, oldengine, oldstructure, oldgear)
					VALUES('$aircraft', '$pirepid', '$enginedamage', '$structuredamage', '$geardamage', '$flightdamage', '$newoverall', '$newengine', '$newstructure', '$newgear', '$oldoverall', '$oldengine', '$oldstructure', '$oldgear')";
					
			DB::query($query);
	}

	public static function updateaircraftstate($aircraft, $newoverall, $newengine, $newstructure, $newgear) {
		$query = "UPDATE aircraftmarket_aircraftlist SET gearstate = '$newgear', totalstate ='$newoverall', enginestate = '$newengine', structurestate = '$newstructure' WHERE aircraftid='$aircraft'";
			
		DB::query($query);
	}

	public static function getaircraftlastcheckhours($aircraft) {
		$sql="SELECT currenthours FROM aircraftmarket_mainthistory WHERE aircraftid = '$aircraft' ORDER BY currenthours DESC LIMIT 1";
		$data = DB::get_row($sql);
	
		return round($data->currenthours);
	}

	public static function getaircraftlastCcheckhours($aircraft) {
		$sql="SELECT currenthours FROM aircraftmarket_mainthistory WHERE aircraftid = '$aircraft' AND checktype = 'C' ORDER BY currenthours DESC LIMIT 1";
		$data = DB::get_row($sql);
	
		return round($data->currenthours);
	}

	public static function getaircraftlastILcheckhours($aircraft) {
		$sql="SELECT currenthours FROM aircraftmarket_mainthistory WHERE aircraftid = '$aircraft' AND checktype = 'IL' ORDER BY currenthours DESC LIMIT 1";
		$data = DB::get_row($sql);
	
		return round($data->currenthours);
	}

	public static function getaircraftlastDcheckhours($aircraft) {
		$sql="SELECT currenthours FROM aircraftmarket_mainthistory WHERE aircraftid = '$aircraft' AND checktype = 'D' ORDER BY currenthours DESC LIMIT 1";
		$data = DB::get_row($sql);
	
		return round($data->currenthours);
	}

	public static function getmaintcompdata($maintcomp) {
		$sql="SELECT * FROM aircraftmarket_maintcompanies WHERE id = '$maintcomp'";
		
		return DB::get_row($sql);
	}

	public static function getcontractors() {
		$sql="SELECT * FROM aircraftmarket_maintcompanies";
		
		return DB::get_results($sql);
	}

	public static function addmaintenancehistory($aircraft, $aircrafthours, $checktype, 
		$today, $until, $comdays, $price, $overallstate) {
			$query = "INSERT INTO aircraftmarket_mainthistory (aircraftid, currenthours, checktype, maintdate, finishdate, maintduration, maintprice, totalstate)
						VALUES('$aircraft', '$aircrafthours', '$checktype', '$today', '$until', '$comdays', '$price', '$overallstate')";
						
			DB::query($query);
	}

	public static function getaircrafttocompletemaint() {
		$now = date('Y-m-d H:i:s');
		$sql="SELECT m.*, a.enabled FROM aircraftmarket_mainthistory m
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON a.id = m.aircraftid
		WHERE m.finishdate = (SELECT MAX(t2.finishdate) FROM aircraftmarket_mainthistory t2 WHERE t2.aircraftid = m.aircraftid) AND m.finishdate 
		<= '$now' AND a.enabled = '0'";
			
		return DB::get_results($sql);
	}

	public static function addexpense($expensedate, $expensename, $expensetype, $price) {
		$query = "INSERT INTO aircraftmarket_expenselog (dateadded, name, type, cost)
			VALUES('$expensedate', '$expensename', '$expensetype', '$price')";
						
		DB::query($query);
	}

	public static function getsoldincome() {
		$sql="SELECT SUM(sellprice) as earnings FROM aircraftmarket_soldaircraft";
		$data = DB::get_row($sql);
		
		return round($data->earnings);
	}

	public static function getcurrentloans() {
		$sql="SELECT SUM(debt) as debt FROM aircraftmarket_loans";
		$data = DB::get_row($sql);
	
		return $data->debt;
	}

	public static function getsumloans() {
		$sql="SELECT SUM(principal) as principal FROM aircraftmarket_loans";
		$data = DB::get_row($sql);
		
		return $data->principal;
	}

	public static function getallloans() {
		$sql="SELECT * FROM aircraftmarket_loans";

		return DB::get_results($sql);
	}

	public static function getcurrentleasingdepots() {
		$sql="SELECT SUM(depot) as depot FROM aircraftmarket_leasing";
		$data = DB::get_row($sql);
		
		return $data->depot;
	}

	public static function getsumdellist() {
		$sql="SELECT SUM(baseprice) as pprice FROM aircraftmarket_deliverylist WHERE transactiontype = 'P'";
		$data = DB::get_row($sql);

		$sql2="SELECT SUM(depot) as ldepot, SUM(leaserate) as lrate FROM aircraftmarket_deliverylist WHERE transactiontype = 'L'";		
		$data2 = DB::get_row($sql2);
		
		$total = $data->pprice + $data2->ldepot + $data2->lrate;
		return $total;
	}

	public static function getmarketexpenses() {
		$sql="SELECT SUM(cost) as cost FROM aircraftmarket_expenselog";
		$data = DB::get_row($sql);
		
		return $data->cost;
	}

	public static function updateloans(){
		$now = date('Ym');

		$sql="SELECT * FROM aircraftmarket_loans WHERE lastpayment < '$now'";
		$datas = DB::get_results($sql);
		if($datas)
			foreach($datas as $data){
				$loanid = $data->id;

				$query = "UPDATE aircraftmarket_loans SET lastpayment = '$now', debt = debt - repaymentmonth WHERE id ='$loanid'";
						
				DB::query($query);

				$expensevalue = $data->interestmonth;
				$expensename = 'Loan ('.$loanid.') monthly interest!';
				$expensedate = gmdate('Ym');
				$expensetype = 'M';
				AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $expensevalue);
			}

		$query2 = "DELETE FROM aircraftmarket_loans WHERE debt <= '0'";
				
		DB::query($query2);
	}
	
	public static function instantupdateloans($loanid, $amount){	
		$now = date('Ym');

		$query = "UPDATE aircraftmarket_loans SET lastpayment = '$now', debt = debt - '$amount' WHERE id ='$loanid'";
		DB::query($query);

		$expensevalue = $amount;
		$expensename = 'Loan ('.$loanid.') manual principal payment!';
		$expensedate = gmdate('Ym');
		$expensetype = 'M';
		AircraftMarketData::addexpense($expensedate, $expensename, $expensetype, $expensevalue);

		$query2 = "DELETE FROM aircraftmarket_loans WHERE debt <= '0'";
		DB::query($query2);

	}

	public static function getleasetopay() {
		$month = gmdate('Ym');
		$sql="SELECT l.*, a.registration as acregistration FROM aircraftmarket_leasing l
		LEFT JOIN aircraftmarket_aircraftlist m ON l.aircraft = m.id
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON m.aircraftid = a.id 
		WHERE lastrate < '$month'";
		return DB::get_results($sql);
	}

	public static function getallleasing() {
		$sql="SELECT l.*, m.aircraftid, w.name as lessorname, a.name as aircraftname, a.registration as acregistration FROM aircraftmarket_leasing l
		LEFT JOIN aircraftmarket_aircraftlist m ON l.aircraft = m.id
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON m.aircraftid = a.id
		LEFT JOIN aircraftmarket_owners w ON w.id = l.lessor";

		return DB::get_results($sql);
	}

	public static function getleasedata($id) {
		$sql="SELECT l.*, m.aircraftid, w.name as lessorname, a.icao as acicao, a.name as aircraftname, a.registration as acregistration FROM aircraftmarket_leasing l
		LEFT JOIN aircraftmarket_aircraftlist m ON l.aircraft = m.id
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON m.aircraftid = a.id
		LEFT JOIN aircraftmarket_owners w ON w.id = l.lessor
		WHERE l.id = '$id'";

		return DB::get_row($sql);
	}

	public static function updateleasing($id, $newvalue) {
		$month = gmdate('Ym');
		$query = "UPDATE aircraftmarket_leasing SET lastrate = '$month', remainingvalue ='$newvalue' WHERE id = '$id'";
			
		DB::query($query);
	}

	public static function createautosettingsleased(){
		$lastupdate = gmdate('dH');
		$acount = self::getaircraftdraftcount();
		$leaseaircraft = ($acount * mt_rand(1,4));
		$leaseaircraft = $leaseaircraft - mt_rand(1,51);
		if($leaseaircraft < '0') {
			$leaseaircraft = "5";
		}

		$checkautosetting = self::checkautosetting();

		if(!$checkautosetting) {
			$query2 = "INSERT INTO aircraftmarket_autosetting (leaseupdate, leaseaircraft)
					VALUES('$lastupdate', '$leaseaircraft')";
							
			DB::query($query2);
		} else {
			$query = "UPDATE aircraftmarket_autosetting SET leaseupdate = '$lastupdate', leaseaircraft ='$leaseaircraft' WHERE leaseupdate < '$lastupdate'";
				
			DB::query($query);
		}
	}

	public static function checkautosetting() {
		$sql="SELECT * FROM aircraftmarket_autosetting";

		return DB::get_row($sql);
	}

	public static function getleasesetting() {
		$sql="SELECT leaseaircraft FROM aircraftmarket_autosetting WHERE id = '1'";
		$data = DB::get_row($sql);

		return $data->leaseaircraft;
	}

	public static function getaircraftdraftcount() {
		$sql="SELECT COUNT(id) as account FROM aircraftmarket_aircraftdrafts";
		$data = DB::get_row($sql);

		return $data->account;
	}

	public static function countoffers() {
		$sql="SELECT COUNT(id) as offcount FROM aircraftmarket_offers";
		$data = DB::get_row($sql);
		
		return $data->offcount;
	}

	public static function getrandaircraft() {
		$sql="SELECT * FROM aircraftmarket_aircraftdrafts ORDER BY RAND() LIMIT 1";
		return DB::get_row($sql);
	}

	public static function getrandomowner() {
		$sql="SELECT id FROM aircraftmarket_owners ORDER BY RAND() LIMIT 1";
		$data = DB::get_row($sql);
		
		return $data->id;
	}

	public static function getrandomicao() {
		$sql="SELECT icao FROM ".TABLE_PREFIX."airports ORDER BY RAND() LIMIT 1";
		$data = DB::get_row($sql);
		
		return $data->icao;
	}

	public static function addnewoffer($aircrafttype, $owner, $location, $age, 
		$baseprice, $immediateprice, $leasingrate, $leasingdepot, $contractruntime, 
		$deliverytime, $condition, $expiredate) {
			$query = "INSERT INTO aircraftmarket_offers (aircrafttypeid, aircraftage, accondition, aircraftlocation, baseprice, immediateprice, leasingdepot, leasingrate, offerexpires, owner, deliverytime)
							VALUES('$aircrafttype', '$age', '$condition', '$location', '$baseprice', '$immediateprice', '$leasingdepot', '$leasingrate', '$expiredate', '$owner', '$deliverytime')";
						
			DB::query($query);
	}

	public static function deleteexpiredoffers() {
		$query = "DELETE FROM aircraftmarket_offers WHERE offerexpires < NOW()";
		DB::query($query);
	}

	public static function deleteofferwithid($id) {
		$query = "DELETE FROM aircraftmarket_offers WHERE id = '$id'";
		DB::query($query);
	}


	public static function getmanufacturers() {
		$sql="SELECT * FROM aircraftmarket_manufacturers ORDER BY name ASC";

		return DB::get_results($sql);
	}

	public static function getmanufacturerbyid($id) {
		$sql="SELECT * FROM aircraftmarket_manufacturers WHERE id ='$id'";
		
		return DB::get_row($sql);
	}

	public static function gettypesformanufacturer($manufact) {
		$sql="SELECT *, name FROM aircraftmarket_aircraftdrafts WHERE manufacturer = '$manufact' GROUP BY name ORDER BY icao ASC";
		
		return DB::get_results($sql);
	}

	public static function gettypeinfo($id) {
		$sql="SELECT t.*, m.name as manufacturer FROM aircraftmarket_aircraftdrafts t 
		LEFT JOIN aircraftmarket_manufacturers m ON t.manufacturer = m.id
		WHERE t.id = '$id' ";

		return DB::get_row($sql);
	}

	public static function gettypeoffers($id) {
		$sql="SELECT o.*, w.name as owner FROM aircraftmarket_offers o
		LEFT JOIN aircraftmarket_aircraftdrafts d ON o.aircrafttypeid = d.id
		LEFT JOIN aircraftmarket_owners w ON w.id = o.owner
		WHERE o.aircrafttypeid = '$id' ";

		return DB::get_results($sql);
	}

	public static function getofferinfo($id) {
		$sql="SELECT o.*, o.owner as ownid, w.name as owner, d.name as acname, d.icao as acicao, d.fullname as acfullname, d.listprice as aclistprice FROM aircraftmarket_offers o
		LEFT JOIN aircraftmarket_aircraftdrafts d ON o.aircrafttypeid = d.id
		LEFT JOIN aircraftmarket_owners w ON w.id = o.owner
		WHERE o.id = '$id' ";

		return DB::get_row($sql);
	}

	public static function processorder($aircraft, $baseprice, 
		$delivery, $type, $lessor ='', $depot='', 
		$leaserate='', $condition='100', $location='') {
			if(!$aircraft){
				echo "Already Processed <hr />";
			} else {
				$query = "INSERT INTO aircraftmarket_deliverylist (aircraft, baseprice, delivery, transactiontype, lessor, depot, leaserate, accondition, aclocation)
						VALUES('$aircraft', '$baseprice', '$delivery', '$type' , '$lessor' , '$depot' , '$leaserate' , '$condition' , '$location')";				
				DB::query($query);
			}
	}

	public static function makefinances() {
		$year = gmdate('Y');
		$all_finances = array();
			
		$months = StatsData::GetMonthsInRange('January 1970', 'December '.$year);
		foreach($months as $month) {
			$date_filter = array("DATE_FORMAT(p.submitdate, '%Y%m') = '".date('Ym', $month)."'");
			$this_filter = $date_filter;
				
			$data = PIREPData::getIntervalData($this_filter);
				
			if(!$data) {
				$data = new stdClass();
				$data->ym = date('Y-m', $month);
				$data->timestamp = $month;
				$data->total = 0;
				$data->revenue = 0;
				$data->gross = 0;
				$data->fuelprice = 0;
				$data->price = 0;
				$data->expenses = 0;
				$data->pilotpay = 0;
			}
				else
				{
					$data = FinanceData::calculateFinances($data[0]);
				}
				
				$all_finances[] = $data;
			}

		$sum   = 0;

		foreach ( $all_finances as $fina ) {
			$sum = $sum + $fina->revenue;
		}
	
		$soldincome = AircraftMarketData::getsoldincome();
		$loans = AircraftMarketData::getcurrentloans();
		$leasedepots = AircraftMarketData::getcurrentleasingdepots();
		$marexpenses = AircraftMarketData::getmarketexpenses();
		$pendinglist= AircraftMarketData::getsumdellist();
		$sum = ((($sum + $soldincome + $loans) - $leasedepots) - $marexpenses);
		$sum = $sum - $pendinglist;
		return round($sum,2);
	}

	public static function getorders() {
		$sql="SELECT o.*, w.name as owner, d.name as acname, d.icao as acicao, d.fullname as acfullname, d.listprice as aclistprice FROM aircraftmarket_deliverylist o
		LEFT JOIN aircraftmarket_aircraftdrafts d ON o.aircraft = d.id
		LEFT JOIN aircraftmarket_owners w ON w.id = o.lessor
		ORDER by o.delivery ASC";
		return DB::get_results($sql);
	}

	public static function getdeliveryinfo($id) {
		$sql="SELECT o.*, w.name as owner, d.name as acname, d.icao as acicao, d.fullname as acfullname, d.listprice as aclistprice FROM aircraftmarket_deliverylist o
		LEFT JOIN aircraftmarket_aircraftdrafts d ON o.aircraft = d.id
		LEFT JOIN aircraftmarket_owners w ON w.id = o.lessor
		WHERE o.id = '$id'
		";
		return DB::get_row($sql);
	}


	public static function addaircrafttofleet($icao, $name, $fullname, $registration, $downloadlink, $imagelink, $range, $weight, $cruise, $maxpax, $maxcargo, $minrank, $ranklevel) {
		if(!$icao) {
			echo "Already Processed <hr />";
		} else {
			$query = "INSERT INTO ".TABLE_PREFIX."aircraft (icao, name, fullname, registration, downloadlink, imagelink, `range` , weight, cruise, maxpax, maxcargo, minrank, ranklevel, enabled)
						VALUES('$icao', '$name', '$fullname', '$registration', '$downloadlink', '$imagelink', '$range', '$weight', '$cruise', '$maxpax', '$maxcargo' , '$minrank' , '$ranklevel' , '1')";
							
			DB::query($query);
			return DB::$insert_id;
		}
	}


	public static function addaircrafttoaircraftlist($fleetid, $base, $accondition) {
		if(!$base) {
			echo "Already Processed <hr />";
		} else {
			$query = "INSERT INTO aircraftmarket_aircraftlist (aircraftid, location, totalstate, structurestate)
						VALUES('$fleetid', '$base', '$accondition', '$accondition')";
							
			DB::query($query);
			return DB::$insert_id;
		}
	}

	public static function addpurchasehistory($acname, $baseprice){
		if(!$acname) {
			echo "Already Processed <hr />";
		} else {
			$query = "INSERT INTO aircraftmarket_purchasehistory (aircrafttype, price) VALUES('$acname', '$baseprice')";
							
			DB::query($query);
		}
	}

	public static function addaircrafttolease($listid, $lessor, $baseprice, $depot, $leaserate, $remaining, $lastdate){
		if(!$lessor) {
			echo "Already Processed <hr />";
		}else {
			$query = "INSERT INTO aircraftmarket_leasing (aircraft, lessor, baseprice, depot, leasingrate, remainingvalue, lastrate)
						VALUES('$listid', '$lessor', '$baseprice', '$depot', '$leaserate', '$remaining', '$lastdate')";
						
			DB::query($query);
		}
	}

	public static function deleteorder($id) {
		$query = "DELETE FROM aircraftmarket_deliverylist WHERE id = '$id'";
						
		DB::query($query);
	}

	public static function searchavailableaircraft($icao) {
		$setting = self::getsettings();
		$needstate = $setting->repairstate + 1;
		$sql=" SELECT a.*, l.totalstate FROM ".TABLE_PREFIX."aircraft a
			LEFT JOIN aircraftmarket_aircraftlist l ON a.id = l.aircraftid
						WHERE a.icao = '$icao' AND (l.totalstate is NULL OR l.totalstate >= '$needstate') AND (l.crashed is NULL OR l.crashed = '0') AND  (l.leasecancelled is NULL OR l.leasecancelled = '0') AND (l.forsale is NULL OR l.forsale = '0') AND (l.leased is NULL OR l.leased = '0') AND (l.forlease is NULL OR l.forlease = '0') AND NOT EXISTS (SELECT * FROM ".TABLE_PREFIX."schedules s
					WHERE a.id = s.aircraft) LIMIT 1";
		return DB::get_row($sql);
	}

	public static function getacwosched() {
		$setting = self::getsettings();
		$needstate = $setting->repairstate + 1;
		$sql=" SELECT a.*, l.totalstate FROM ".TABLE_PREFIX."aircraft a
			LEFT JOIN aircraftmarket_aircraftlist l ON a.id = l.aircraftid
						WHERE (l.totalstate is NULL OR l.totalstate >= '$needstate') AND (l.crashed is NULL OR l.crashed = '0') AND  (l.leasecancelled is NULL OR l.leasecancelled = '0') AND (l.forsale is NULL OR l.forsale = '0') AND (l.leased is NULL OR l.leased = '0') AND (l.forlease is NULL OR l.forlease = '0') AND NOT EXISTS (SELECT * FROM ".TABLE_PREFIX."schedules s
					WHERE a.id = s.aircraft) ORDER BY a.icao, a.registration";
		return DB::get_results($sql);
	}

	public static function getacwsched() {
		$sql=" SELECT a.*, l.totalstate FROM ".TABLE_PREFIX."aircraft a
			LEFT JOIN aircraftmarket_aircraftlist l ON a.id = l.aircraftid
						WHERE EXISTS (SELECT * FROM ".TABLE_PREFIX."schedules s
					WHERE a.id = s.aircraft) ORDER BY a.icao, a.registration";
		return DB::get_results($sql);
	}

	public static function switchacschedules($newid, $oldid) {
		$query = "UPDATE ".TABLE_PREFIX."schedules SET aircraft='$newid' WHERE aircraft='$oldid'";
			
		DB::query($query);
	}

	public static function deleteschedulesbyac($id) {
		$query = "DELETE FROM ".TABLE_PREFIX."schedules WHERE aircraft = '$id'";
						
		DB::query($query);
	}
	
	public static function updatelistinfolease($aircraft, $status) {
		$query = "UPDATE aircraftmarket_aircraftlist SET leasecancelled = '$status' WHERE id='$aircraft'";
				
		DB::query($query);
	}

	public static function updatelistinfosale($aircraft, $status) {
		$query = "UPDATE aircraftmarket_aircraftlist SET forsale = '$status' WHERE id='$aircraft'";
			
		DB::query($query);
	}

	public static function deleteleasing($id) {
		$query = "DELETE FROM aircraftmarket_leasing WHERE id = '$id'";
						
		DB::query($query);
	}

	public static function deletefinishedlease() {
		$query = "DELETE FROM aircraftmarket_leasing WHERE remainingvalue <= '0'";
						
		DB::query($query);

	}


	public static function disablesoldandleased() {
		$sql="SELECT l.* FROM aircraftmarket_aircraftlist l 
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON a.id = l.aircraftid
		WHERE l.forlease = '1' OR l.forsale = '1' OR l.leased = '1' OR l.leasecancelled = '1' AND a.enabled ='1'";
		$data = DB::get_row($sql);

		self::disableaircraft($data->aircraftid);
		self::disableschedules($data->aircraftid);
	}

	public static function disableallcrashed() {
		$sql="SELECT l.* FROM aircraftmarket_aircraftlist l 
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON a.id = l.aircraftid
		WHERE l.crashed = '1' AND a.enabled ='1'";
		$data = DB::get_row($sql);

		self::disableaircraft($data->aircraftid);
		self::disableschedules($data->aircraftid);
	}

	public static function savemanufacturer($name) {
		$query = "INSERT INTO aircraftmarket_manufacturers (name) VALUES('$name')";
						
		DB::query($query);
	}

	public static function editmanufacturer($id, $name) {
		$query = "UPDATE aircraftmarket_manufacturers SET name = '$name' WHERE id='$id'";
			
		DB::query($query);
	}

	public static function savetype($manufacturer, $icao, $name, $fullname, $downloadlink, $imagelink, $range, $weight, $cruise, $maxpax, $maxcargo, $minrank, $ranklevel, $serviceceiling, $listprice, $productionfrom, $productionto) {
		if(!$icao) {
			echo "Already Added <hr />";
		} else {
			$query = "INSERT INTO aircraftmarket_aircraftdrafts (manufacturer, icao, name, fullname, downloadlink, imagelink, `range` , weight, cruise, maxpax, maxcargo, minrank, ranklevel, serviceceiling, listprice, productionfrom, productionto)
						VALUES('$manufacturer', '$icao', '$name', '$fullname', '$downloadlink', '$imagelink', '$range', '$weight', '$cruise', '$maxpax', '$maxcargo' , '$minrank' , '$ranklevel' , '$serviceceiling', '$listprice', '$productionfrom', '$productionto')";
						
			DB::query($query);
		}
	}

	public static function edittype($id, $icao, $name, $fullname, $downloadlink, $imagelink, $range, $weight, $cruise, $maxpax, $maxcargo, $minrank, $ranklevel, $serviceceiling, $listprice, $productionfrom, $productionto) {
		$query = "UPDATE aircraftmarket_aircraftdrafts SET icao = '$icao', name = '$name', fullname = '$fullname', downloadlink = '$downloadlink', imagelink = '$imagelink', `range` = '$range', weight = '$weight', cruise = '$cruise', maxpax = '$maxpax', maxcargo = '$maxcargo', minrank = '$minrank', ranklevel = '$ranklevel', serviceceiling = '$serviceceiling', listprice = '$listprice', productionfrom = '$productionfrom', productionto = '$productionto' WHERE id='$id'";
			
		DB::query($query);
	}
		
	public static function checkinmaintenance($aircraft) {
		$now = date('Y-m-d H:i:s');
			
		$sql="SELECT * FROM aircraftmarket_mainthistory
		WHERE aircraftid = '$aircraft'
		AND finishdate > '$now'";

		return DB::get_row($sql);
	}

	public static function getcrashedaircraft() {
		$sql="SELECT a.*, l.aircraftid FROM ".TABLE_PREFIX."aircraft a 
		LEFT JOIN aircraftmarket_aircraftlist l ON l.aircraftid = a.id
		WHERE l.crashed = '1' ";

		return DB::get_results($sql);
	}

	public static function getaircraftinmaintenance() {
		$now = date('Y-m-d H:i:s');
			
		$sql="SELECT a.*, a.id as acid, l.aircraftid as acid, m.finishdate, m.aircraftid, m.checktype, m.totalstate FROM aircraftmarket_mainthistory m
		LEFT JOIN ".TABLE_PREFIX."aircraft a ON m.aircraftid = a.id
		LEFT JOIN aircraftmarket_aircraftlist l ON l.aircraftid = a.id
		WHERE m.finishdate > '$now'
		GROUP BY m.aircraftid
		ORDER BY m.finishdate ASC";

		return DB::get_results($sql);
	}

	public static function getavailaircraft() {
		$sql="SELECT a.*, a.id as acid, l.*, l.id as listid FROM ".TABLE_PREFIX."aircraft a 
		LEFT JOIN aircraftmarket_aircraftlist l ON l.aircraftid = a.id
		WHERE (l.crashed is NULL OR l.crashed = '0') AND  (l.leasecancelled is NULL OR l.leasecancelled = '0') AND (l.forsale is NULL OR l.forsale = '0') AND (l.leased is NULL OR l.leased = '0') AND (l.forlease is NULL OR l.forlease = '0') ORDER BY a.icao, a.registration";
		
		return DB::get_results($sql);
	}

	public static function getacinfo($id) {
		$sql="SELECT a.*, l.aircraftid as aircrid, l.*, l.id as listid, a.id as acid, d.listprice FROM ".TABLE_PREFIX."aircraft a 
		LEFT JOIN aircraftmarket_aircraftlist l ON l.aircraftid = a.id
		LEFT JOIN aircraftmarket_aircraftdrafts d ON d.icao = a.icao
		WHERE a.id = '$id' ";

		return DB::get_row($sql);
	}

	public static function getmainthisto($id) {
		$sql="SELECT * FROM aircraftmarket_mainthistory
		WHERE aircraftid = '$id' ORDER BY maintdate DESC";

		return DB::get_results($sql);
	}

	public static function getdamagehis($id) {
		$sql="SELECT d.*, p.pilotid, p.code, p.flightnum, p.depicao, p.arricao, p.landingrate, i.firstname, i.lastname, p.submitdate FROM aircraftmarket_damagehistory d
		LEFT JOIN ".TABLE_PREFIX."pireps p ON p.pirepid = d.pirepid
		LEFT JOIN ".TABLE_PREFIX."pilots i ON i.pilotid = p.pilotid
		WHERE d.aircraftid = '$id' ORDER BY d.id DESC";
		
		return DB::get_results($sql);
	}

	public static function checkleasing($id) {
		$sql="SELECT * FROM aircraftmarket_leasing
		WHERE aircraft = '$id'";

		return DB::get_results($sql);
	}


	public static function addsoldaircraft($oldid, $sellprice, $gearcond, $structurecond, $enginecond, $totalcond) {
		if(!$oldid) {
			echo "Already Sold <hr />";
		} else {
			$query = "INSERT INTO aircraftmarket_soldaircraft (aircraftid, sellprice, gearstate, enginestate, structurestate, totalstate)
						VALUES('$oldid', '$sellprice', '$gearcond', '$enginecond', '$structurecond', '$totalcond')";
							
			DB::query($query);
		}
	}


	public static function addloan($principal, $debt, $repaymentmonth, $interest, $interestmonth, $lastpay) {
		if(!$principal) {
			echo "Already Added <hr />";
		} else {
			$query = "INSERT INTO aircraftmarket_loans (principal, debt, repaymentmonth, interest, interestmonth, lastpayment)
				VALUES('$principal', '$debt', '$repaymentmonth', '$interest', '$interestmonth', '$lastpay')";
						
			DB::query($query);
		}
	}

	public static function getconditionforpirep($pirepid) {
		$sql="SELECT *, newstate as totalcond, newengine as enginecond, newgear as gearcond, newstructure as structurecond, oldstate as oldtotalcond, oldgear as oldgearcond, oldengine as oldenginecond, oldstructure as oldstructurecond FROM aircraftmarket_damagehistory
			WHERE pirepid = '$pirepid'";
		return DB::get_row($sql);
	}

	public static function getaccond($aircraftid) {
		$sql="SELECT * FROM aircraftmarket_aircraftlist WHERE aircraftid = '$aircraftid'";
		$data = DB::get_row($sql);

		return $data->totalstate;
	}

	public static function getacgearcond($aircraftid) {
		$sql="SELECT * FROM aircraftmarket_aircraftlist WHERE aircraftid = '$aircraftid'";
		$data = DB::get_row($sql);

		return $data->gearstate;
	}

	public static function getacenginecond($aircraftid) {
		$sql="SELECT * FROM aircraftmarket_aircraftlist WHERE aircraftid = '$aircraftid'";
		$data = DB::get_row($sql);
		
		return $data->enginestate;
	}

	public static function getacstructurecond($aircraftid) {
		$sql="SELECT * FROM aircraftmarket_aircraftlist WHERE aircraftid = '$aircraftid'";
		$data = DB::get_row($sql);

		return $data->structurestate;
	}


}	
 ?>