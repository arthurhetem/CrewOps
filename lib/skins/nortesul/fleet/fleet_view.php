<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Aircraft Information for <?php echo $basicinfo->registration; ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item">Fleet</li>
                    <li class="breadcrumb-item active"><?php echo $basicinfo->registration; ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Basic Information</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td rowspan="4" width="50%" style="text-align: left"><img src="<?php echo $basicinfo->imagelink; ?>" alt="No Images Available" class="img-fluid" style="border-radius: 0.8rem;"/></td>
                                        <td style="text-align: left"><strong>Type: </strong> <?php echo $basicinfo->fullname; ?></td>
                                        <td style="text-align: left"><strong>Range: </strong> <?php echo $basicinfo->range; ?> <i>miles</i></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left"><strong>Max Cargo: </strong> <?php echo $basicinfo->maxcargo; ?> <i>lbs</i></td>
                                        <td style="text-align: left"><strong>Max Passengers: </strong> <?php echo $basicinfo->maxpax; ?> <i>pax</i></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left"><strong>Weight: </strong> <?php echo $basicinfo->weight; ?> <i>lbs</i></td>
                                        <td style="text-align: left"><strong>Max Cruise Alt: </strong> <?php echo $basicinfo->cruise; ?> <i>ft</i></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Detailed Information</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><strong>Average fuel use per flight: </strong><?php echo round($detailedinfo['AvgFuel'], 2); ?> <i>lbs</i></td>
                                        <td><strong>Total fuel used: </strong> <?php echo round($detailedinfo['totalFuel'], 2); ?> <i>lbs</i></td>
                                        <td><strong>Fuel consumption rate: </strong> <?php echo round($detailedinfo['fuelConsumption'], 2); ?> <i>lbs/mile</i></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Average Flight Distance per flight: </strong> <?php echo round($detailedinfo['fuelConsumption'], 2); ?><i>miles</i></td>
                                        <td><strong>Total Flight Distance: </strong> <?php echo round($detailedinfo['TotalFlightDistance'], 2); ?> <i>miles</i></td>
                                        <td><strong>Average Revenue: </strong> $<?php echo round($detailedinfo['AvgRevenue'], 2); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Total Revenue: </strong> $<?php echo round($detailedinfo['totalRevenue'], 2); ?></td>
                                        <td><strong>Total Expenses: </strong>$<?php echo round($detailedinfo['totalExpenses'], 2); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Latest Flights</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="row">Flight Number</th>
                                        <th scope="row">Departure</th>
                                        <th scope="row">Arrival</th>
                                        <th scope="row">Pilot In Command</th>
                                        <th scope="row">Distance</th>
                                        <th scope="row">Revenue</th>
                                        
                                        <th>Landing Rate</th>
                                    </tr>
                                </thead>
                                <tbody><?php if($recentflights != null){foreach($recentflights as $recentflight){ ?>
                                    <tr>
                                        <td><a href="<?php echo url('pireps/view/' . $recentflight->pirepid); ?>/" ><?php echo $recentflight->code . " " . $recentflight->flightnum; ?></a></td>
                                        <td><?php echo $recentflight->depicao; ?></td>
                                        <td><?php echo $recentflight->arricao; ?></td>
                                        <td><?php echo PilotData::getPilotData($recentflight->pilotid)->firstname. " " .PilotData::getPilotData($recentflight->pilotid)->lastname; ?></td>
                                        <td><?php echo $recentflight->distance; ?> <i>miles</i></td>
                                        <td style="color:<?php if($recentflight->revenue >0){ echo 'green'; }else{ echo 'red'; } ?> ;">$<?php echo $recentflight->revenue; ?></td>
                                    
                                        <td>-<?php echo $recentflight->landingrate; ?> ft/min</td>
                                        
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Scheduled Flights</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>Flight Number</th>
                                        <th>Departure</th>
                                        <th>Arrival</th>
                                        <th>Dep time</th>
                                        <th>Arr time</th>
                                        <th>Distance</th>
                                        <th>Flight Duration</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($scheduledflights != null){foreach($scheduledflights as $scheduledflights){ ?>
                                    <tr>
                                        <td><?php echo $scheduledflights->code . " " . $scheduledflights->flightnum; ?></td>
                                        <td><?php echo $scheduledflights->depicao; ?></td>
                                        <td><?php echo $scheduledflights->arricao; ?></td>
                                        <td><?php echo $scheduledflights->deptime; ?></td>
                                        <td><?php echo $scheduledflights->arrtime; ?></td>
                                        <td><?php echo $scheduledflights->distance; ?> <i>miles</i></td>
                                        <td><?php echo $scheduledflights->flighttime; ?></td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Aircraft Rentability</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                    <?php
					$revenue_1 = $recentflights[0]->revenue;
					$revenue_2 = $recentflights[1]->revenue;
					$revenue_3 = $recentflights[2]->revenue;
					$revenue_4 = $recentflights[3]->revenue;
					$revenue_5 = $recentflights[4]->revenue;
				?>
                <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.bundle.min.js"></script>
                <canvas id="myChart" width="400" height="150"></canvas>
<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['1', '2', '3', '4', '5'],
        datasets: [{
            label: 'Revenue',
            data: [<?php echo $revenue_1 ; ?>, <?php echo $revenue_2 ; ?>, <?php echo $revenue_3 ; ?>, <?php echo $revenue_4 ; ?>, <?php echo $revenue_5 ; ?>],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#info').addClass('active');
    $('#fleet').addClass('active');
</script>