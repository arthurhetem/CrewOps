<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Fleet</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item active">Fleet</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <table class="table" id="fleetTable">
                            <thead>
                                <tr>
                                    <th>Aircraft Registration</th>
                                    <th>Aircraft Type</th>
                                    <th>Range</th>
                                    <th>Max Passengers</th>
                                    <th>Max Cargo</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if($aircrafts != null){ foreach($aircrafts as $aircrafts){
                                $pos = stripos($aircrafts->registration, "NSV");
                                if ($pos !== false) {
                                    continue;
                                    }?>
                                <tr>
                                    <td><a href="<?php echo url('fleet/view/' . $aircrafts->id); ?>"><?php echo $aircrafts->registration; ?></a></td>
                                    <td><?php echo $aircrafts->fullname; ?></td>
                                    <td><?php echo $aircrafts->range; ?><i> miles</i></td>
                                    <td><?php echo $aircrafts->maxpax; ?></td>
                                    <td><?php echo $aircrafts->maxcargo; ?></td>
                                </tr>
                            <?php } }?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $('#info').addClass('active');
    $('#fleet').addClass('active');
    $(window).load(function () {
        $('#fleetTable').DataTable({
        "lengthChange": false,
        "ordering": true,
        "autoWidth": false,
        "responsive": true,
        "paging": false,
        "searching": true,
        });
  });
</script>