<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Leaderboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item active"><a href="<?php echo SITE_URL; ?>/TopPilot">Leaderboard</a></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<?php $month_name = date( 'F', mktime(0, 0, 0, $month) );?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-warning card-outline card-tabs">
                    <div class="card-header">
                        <h3 class="card-title">Leaderboard at <?php echo $month_name. ' '. $year; ?></h3>
                    </div>
                    <div class="card-body p-0">
                        <div class="alert alert-primary">Flights Flown</div>
                        <table class="table mb-3">
                            <thead>
                                <tr>
                                    <th scope="row">Pilot</th>
                                    <th scope="row">Flights Flown</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($topflights as $top) {
                                        $pilot = PilotData::GetPilotData($top->pilot_id);
                                    ?>
                                <tr>
                                    <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::getPilotCode($pilot->code, $pilot->pilotid); ?>
                                    </td>
                                    <td><?php echo $top->flights; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="alert alert-success">Hours Flown</div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="row">Pilot</th>
                                    <th scope="row">Hours Flown</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                    foreach ($tophours as $top) {
                                        $pilot = PilotData::GetPilotData($top->pilot_id);
                                ?>
                                <tr>
                                    <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>
                                    </td>
                                    <td><?php echo $top->hours; ?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<script>
    $('#leaderboard').addClass('active');
</script>