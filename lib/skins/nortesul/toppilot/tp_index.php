<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Leaderboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item active">Leaderboard</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline card-tabs">
                    <div class="card-header p-0 pt-1 border-bottom-0">
                        <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill"
                                    href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home"
                                    aria-selected="true">All Time</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill"
                                    href="#custom-tabs-three-profile" role="tab"
                                    aria-controls="custom-tabs-three-profile" aria-selected="false">This Month</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill"
                                    href="#custom-tabs-three-messages" role="tab"
                                    aria-controls="custom-tabs-three-messages" aria-selected="false">This Year</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body p-0">
                        <div class="tab-content" id="custom-tabs-three-tabContent">
                            <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel"
                                aria-labelledby="custom-tabs-three-home-tab">
                                <div class="alert alert-primary">Flights Flown</div>
                                <table class="table mb-3">
                                    <thead>
                                        <tr>
                                            <th scope="row">Pilot</th>
                                            <th scope="row">Flights Flown</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $all_flights = TopPilotData::alltime_flights(5);
                                        foreach($all_flights as $all) {
                                        $pilot = PilotData::GetPilotData($all->pilotid);
                                        ?>
                                        <tr>
                                            <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>
                                            </td>
                                            <td><?php echo $all->totalflights; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="alert alert-primary">Hours Flown</div>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="row">Pilot</th>
                                            <th scope="row">Hours Flown</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $all_hours = TopPilotData::alltime_hours(5);
                                        foreach($all_hours as $all) {
                                        $pilot = PilotData::GetPilotData($all->pilotid);
                                        ?>
                                        <tr>
                                            <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>
                                            </td>
                                            <td><?php echo $all->totalhours; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel"
                                aria-labelledby="custom-tabs-three-profile-tab">
                                <?php
                                $topflights = TopPilotData::top_pilot_flights($today["mon"], $today["year"], 5);
                                if(!$topflights) {
                                        $month = date( 'F', mktime(0, 0, 0, date("m"))); 
                                        echo '<div class="alert alert-info"><h4><i class="fas fa-exclamation-triangle"></i> Oops</h4>No Pireps Filed For '.$month.' '.$today['year'].'</div>';
                                    } else {
                                        $month_name = date( 'F', mktime(0, 0, 0, $topflights[0]->month) );
                                ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="alert alert-primary">Top Pilot for
                                            <?php echo $month_name.' '.$topflights[0]->year; ?> (Flights Flown)</div>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="row">Pilots</th>
                                                    <th scope="row">Flights Flown</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                    foreach ($topflights as $top) {
                                        $pilot = PilotData::GetPilotData($top->pilot_id);
                                ?>
                                                <tr>
                                                    <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>
                                                    </td>
                                                    <td><?php echo $top->flights; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="alert alert-primary">Top Pilot for
                                            <?php
                                            $tophours = TopPilotData::top_pilot_hours($today['mon'], $today['year'], 5);
                                            echo $month_name.' '.$tophours[0]->year; ?> (Hours Flown)</div>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="row">Pilots</th>
                                                    <th scope="row">Hours Flown</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    foreach ($tophours as $top) {
                                                    $pilot = PilotData::GetPilotData($top->pilot_id);
                                                ?>
                                                <tr>
                                                    <td><?php echo $pilot->firstname.' '.$pilot->lastname.' - '.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>
                                                    </td>
                                                    <td><?php echo $top->hours; ?></td>
                                                </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel"
                                aria-labelledby="custom-tabs-three-messages-tab">
                                <table class="table">
                                    <?php
                        while ($startyear <= $today['year']): {
                            $month_name = date( 'F', mktime(0, 0, 0, $startmonth) );
                    ?>
                                    <tr>
                                        <td><?php echo $month_name.' - '.$startyear; ?></td>
                                        <td><a
                                                href="<?php echo url('/TopPilot/get_old_stats?month='.$startmonth.'&year='.$startyear.''); ?>">View</a>
                                        </td>
                                    </tr>
                                    <?php
                            //advance dates
                            if ($startmonth == $today['mon'] && $startyear == $today['year']) {break;}
                            if ($startmonth == 12) {
                                $startyear++; $startmonth = 01;
                            } else {
                                $startmonth++;
                            }
                        } endwhile;
                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#leaderboard').addClass('active');
</script>