<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Roster Generator</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv OPS&trade;</a></li>
                    <li class="breadcrumb-item">Flight Operations</li>
                    <li class="breadcrumb-item active">Roster Generator</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Avaliable Itinerary</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive">
                        <div class="alert alert-success"><h5>Attention</h5>If you are not comfortable with the generated roster, you can always regenerate.</div>
                        <table class="table table-hover table-light table-bordered" id="tabela">
                            <thead>
                                <tr>
                                    <th> <center>Flight #</center></th>
								    <th> <center>Departure</center></th>
                                    <th> <center>Arrival</center></th>
                                    <th> <center>Registration</center></th>
                                    <th> <center>Duration</center></th>
                                </tr>
                            </thead>
							<tbody>
							    <?php
                                    $pilotid = Auth::PilotID();
                                    $user = PilotData::getPilotData($pilotid);

                                    if (!$schedules)
                                        { ?>
                                            <span class="text-red">Unable to generate roster, try altering the parameters.!</span>
                                            <?php
                                        }else{
                                    foreach($schedules as $result){
                                                $info = OperationsData::getAircraftByReg($result->registration);
                                            ?>
                                        <tr>
                                            <td><center><?php echo $result->code.$result->flightnum;?></center></td>
                                            <td><center><?php echo $result->depicao;?></center></td>
                                            <td><center><?php echo $result->arricao;?></center></td>
                                            <td><center><?php echo $info->registration;?></center></td>
                                            <td><center><?php echo $result->flighttime;?> Hour(s)</center></td>
                                        </tr>
                                    <?php 	}
                                        } 
                                        $countOfSchedules = (is_array($schedules) ? count($schedules) : 0);
                                        ?>
							</tbody>
                        </table>
                        <form name="bidAll" id="bidAll" action="<?php echo SITE_URL?>/randomflights/bidAll" method="post">
	                        <input type="hidden" name="count" value = "<?php echo $countOfSchedules;?>"/>
	                        <input type="hidden" name="pilotid" value="<?php echo $pilotid;?>"/>
                            <?php
                                for($i = 0; $i < $countOfSchedules; $i++)
                                {
                                    ?>
                                    <input type="hidden" name="schedules[<?php echo $i;?>]" value="<?php echo $schedules[$i]->id;?>">
                                <?php
                                }
                                ?>
                                <p>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="hidden" name="equip" value="<?php echo $equip;?>">
                                            <input type="submit" name="submit" value="Bid Roster" type="button" class="btn btn-success btn-block">
                                        </div>
                                    </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->