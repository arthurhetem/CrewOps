<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Airports</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item active">Airports</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Airport Information</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table" id="airportsTable">
					        <thead>
                                <tr>
                                    <th>ICAO</th>
                                    <th>Airport Name</th>
                                    <th>Airport Country</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $allairports = OperationsData::GetAllAirports();
                                    foreach ($allairports as $airport) {
                                ?>
                                    <tr>
                                        <td><?php echo '<a href=" '.SITE_URL.'/index.php/airports/get_airport?icao='.$airport->icao.'">'.$airport->icao.'</a>';?></td>
                                        <td><?php echo $airport->name; ?> </td>
                                        <td><?php echo $airport->country; ?></td>
                                    </tr>
					            <?php } ?>
					        </tbody>
				        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#info').addClass('active');
    $('#airports').addClass('active');
    $(window).load(function () {
    $('#airportsTable').DataTable({
      "lengthChange": false,
      "ordering": true,
      "autoWidth": false,
      "responsive": true,
      "paging": false,
      "searching": false,
    });
  });
</script>