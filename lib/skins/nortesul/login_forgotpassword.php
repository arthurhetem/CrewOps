<div class="background-login"></div>
<div class="login-content animate__animated animate__fadeInRight">
  <div class="login-logo">
    <img src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/img/logo.png" alt="">
  </div>
  <!-- /.login-logo -->

  <form action="<?php echo url('/login/forgotpassword');?>" method="post">
    <div class="input-group mb-4">
      <input type="text" class="form-control" name="email" placeholder="Email">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-envelope"></span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-6">
        <a href="<?php echo SITE_URL; ?>/login"><button type="submit" class="btn btn-outline-primary btn-block"><i
              class="fas fa-arrow-left"></i> Go Back</button></a>
      </div>
      <div class="col-6">
        <input type="hidden" name="action" value="resetpass" />
        <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt"></i> Request new
          password</button>
      </div>
      <!-- /.col -->
    </div>
  </form>
  <div class="col-xs-12 text-center mt-10">
    <small>© <?php echo date("Y");?> NorteSul Virtual. All rights reserved.</small>
  </div>
</div>
<!-- /.login-card-body -->