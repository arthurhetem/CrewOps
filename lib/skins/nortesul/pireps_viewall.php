<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Logbook for <?php echo Auth::$userinfo->firstname;?> <?php echo Auth::$userinfo->lastname;?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item active">Pilots</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Post Flight Report List</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body <?php if(!$pireps){ } else { echo 'p-0'; }?>">
                <?php if(!$pireps) { ?>
                    <div class="alert alert-danger"><h5><i class="icon fas fa-exclamation-triangle"></i> No Reports Found</h5>You have not filed any reports.</div>
            <?php } else { ?>
            <?php require 'flown_routes_map.php'; ?>
                    <div class="table-responsive">
                    <table class="table mt-3" id="logbookTable">
                        <thead>
                            <tr>
                                <th scope="row">Flight Number</th>
                                <th scope="row">Departure</th>
                                <th scope="row">Arrival</th>
                                <th scope="row">Aircraft</th>
                                <th scope="row">Flight Time</th>
                                <th scope="row">Submitted</th>
                                <th scope="row">Status</th>
                                <?php
                                    // Only show this column if they're logged in, and the pilot viewing is the owner/submitter of the PIREPs
                                    if(Auth::LoggedIn() && Auth::$userinfo->pilotid == $userinfo->pilotid) {
                                        echo '<th>Options</th>';
                                    }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($pireps as $report) {
                            ?>
                            <tr>
                                <td><a href="<?php echo url('/pireps/view/'.$report->pirepid);?>"><?php echo $report->code . $report->flightnum; ?></a></td>
                                <td><?php echo $report->depicao; ?></td>
                                <td><?php echo $report->arricao; ?></td>
                                <td><?php echo $report->aircraft . " ($report->registration)"; ?></td>
                                <td><?php echo $report->flighttime; ?></td>
                                <td><?php echo date("d/M/Y", $report->submitdate); ?></td>
                                <td>
                                    <?php
                                    if($report->accepted == PIREP_ACCEPTED)
                                        echo '<div id="success" class="badge badge-success">Accepted</div>';
                                    elseif($report->accepted == PIREP_REJECTED)
                                        echo '<div id="error" class="badge badge-danger">Rejected</div>';
                                    elseif($report->accepted == PIREP_PENDING)
                                        echo '<div id="error" class="badge badge-info">Approval Pending</div>';
                                    elseif($report->accepted == PIREP_INPROGRESS)
                                        echo '<div id="error" class="badge badge-warning">Flight in Progress</div>';
                                    ?>
                                </td>
                                <?php
                                    // Only show this column if they're logged in, and the pilot viewing is the owner/submitter of the PIREPs
                                    if(Auth::LoggedIn() && Auth::$userinfo->pilotid == $report->pilotid) {
                                ?>
                                <td>
                                    <a href="<?php echo url('/pireps/editpirep?id='.$report->pirepid);?>">Edit PIREP</a>
                                </td>
                                <?php } ?>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                    <!-- /.table-responsive -->
                    <?php } ?>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#ops').addClass('active');
    $('#logbook').addClass('active');
    $(window).load(function () {
        $('#logbookTable').DataTable({
        "lengthChange": false,
        "ordering": false,
        "autoWidth": false,
        "responsive": true,
        "paging": false,
        "searching": false,
        });
  });
</script>