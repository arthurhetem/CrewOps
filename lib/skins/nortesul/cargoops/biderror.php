<div class="alert alert-danger">
<h4><i class="icon fas fa-exclamation-triangle"></i> Error booking flight!</h4>
Try again later, or contact a Staff member
</div>
<script type='text/javascript'>$(window).load(function(){
    Swal({
  title: 'Error adding bid!',
  text: 'Try again later, or contact a Staff member',
  icon: 'error',
  heightAuto: true
})
  });</script>