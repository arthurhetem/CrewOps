<?php
  if (!$pireps) {?>
    <div class="alert alert-danger">
      <h5><i class="icon fas fa-exclamation-triangle"></i> Oops</h5>
      Looks like you haven't flown any freight yet :( Don't worry, we'll have something to show once you have flown!
    </div>
  </div>
</section>
<?php 
    return;
  }
?>
<div class="row">
    <div class="col-md-12">
      <div class="card">
      <div class="card-header with-border">
      <h3 class="card-title">Cargo Operations Logbook</h3>
      </div>
      <div class="card-body p-0 table-responsive">
        <table class="table table-striped table-hover">
          <thead>
          <th>Flight#</th>
          <th>Departure</th>
          <th>Arrival</th>
          <th>Aircraft</th>
          <th>Duration</th>
          <th>Landing Rate</th>
          <th>Date</th>
          </thead>
          <?php
          foreach($pireps as $pirep)
          {
          ?>
          <tr>
          <td><?php echo $pirep->code; ?><?php echo $pirep->flightnum; ?></td>
          <td><?php echo $pirep->depicao; ?></td>
          <td><?php echo $pirep->arricao; ?></td>
          <td><?php echo $pirep->name; ?> (<?php echo $pirep->registration; ?>)</td>
          <td><?php echo $pirep->flighttime; ?></td>
          <td><?php echo $pirep->landingrate; ?></td>
          <td><?php echo $pirep->submitdate; ?></td>
          </tr>
          <?php
          }
          ?>
          </table>
      </div>
    </div>
    </div>
</div>

</div>
</section>