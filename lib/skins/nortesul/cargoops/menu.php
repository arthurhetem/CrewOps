<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <div class="col-12 text-center">
              <a href="<?php echo SITE_URL ?>/CargoOps" class="btn btn-app">
                <i class="fas fa-plane"></i> Available Contracts
              </a>
              <a href="<?php echo SITE_URL ?>/CargoOps/history" class="btn btn-app">
                <i class="fas fa-boxes"></i> Transported Cargo
              </a>
              <a href="<?php echo SITE_URL ?>/CargoOps/fleet" class="btn btn-app">
                <i class="fas fa-paper-plane"></i> Cargo Fleet Status
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>