<?php $contractcount = is_array($contracts) ? count($contracts) : 0; ?>
<?php if($contractcount < 1) {
?>
    <div class="alert alert-info">
    <h5><i class="icon fas fa-info"></i> Oops</h5>
      There aren't any contracts here</div>
  </div>
</section>
<?php 
  return;
}
?>

<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header with-border">
          <h3 class="card-title">Available Contracts</h3>
          <div class="pull-right card-tools">
            <span class="badge badge-success">Contracts: <?php echo $contractcount; ?></span>
          </div>
        </div>
        <div class="card-body p-0 table-responsive">
          <table class="table table-striped table-hover">
          <thead>
          <tr>
          <td>Flight#</td>
          <td>Departure</td>
          <td>Arrival</td>
          <td width="100px">Aircraft</td>
          <td>Duration</td>
          <td>Cargo</td>
          <td>Weight</td>
          <td>Estimated Profit</td>

          <?php if(Auth::LoggedIn())
          {
            $location = FltbookData::getLocation(Auth::$userinfo->pilotid);
          ?>
          <td>Disponibility</td>
          <?php } ?>
          </tr>
        </thead>
          <?php foreach($contracts as $contract){
            if ($contract->depicao !== $location->arricao)
              continue;
          ?>
          <tr>
          <td valign="top"><?php echo $contract->code; echo $contract->flightnum; ?></td>
          <td valign="top"><strong><?php echo ucwords($contract->depicao); ?></strong>
          <br/>
          <?php echo $contract->depname; ?>
          </td>
          <td valign="top"><strong><?php echo ucwords($contract->arricao); ?></strong>
          <br/>
          <?php echo $contract->arrname; ?>
          </td>
          <td valign="top"><?php echo $contract->aircraftname; ?></td>
          <td valign="top"><?php echo $contract->flighttime; ?></td>
          <td valign="top"><?php echo $contract->cargoname; ?></td>
          <td valign="top" class="text-green"><?php $peso = $contract->cload; echo round($peso/2.205, -1);?>kgs</td>
          <td valign="top">$<?php echo number_format(round($contract->cload * $contract->price)); ?></td>
          <?php if(Auth::LoggedIn() && Auth::$userinfo->ranklevel >= $contract->aircraftlevel){ ?>
          <td valign="top"><a href="<?php echo SITE_URL ?>/CargoOps/contractdetails/<?php echo $contract->cid; ?>" class="btn btn-sm btn-success">Details</a></td>
        <?php } else{ ?>
          <td valign="top"><a href="#" class="btn btn-danger" data-toggle="tooltip" title="At this moment you can't bid any cargo contracts, try again later" disabled>Details</a></td>
        <?php }?>
          </tr>
        <?php }?>
          </table>
        </div>
    </div>
    </div>
</div>


      </div>
</section>
