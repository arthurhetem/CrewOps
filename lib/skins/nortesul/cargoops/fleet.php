<section class="content container-fluid">
    <div class="row">
        <div class="col-md-12">
          <div class="card card-widget widget-user">
          <!-- Add the bg color to the header using any of the bg-* classes -->
          <div class="widget-user-header bg-black">
            <h3 class="widget-user-username text-center">Our <strong>Cargo</strong> Fleet</h3>
          </div>
          <div class="card-footer">
                         <div class="row">
            <?php
            if(!$aircraft)
            {
            return;
            }
             foreach($aircraft as $ac)
            {
            ?>
            <div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-success"><i class="fas fa-plane fa-success"></i></span>

              <div class="info-box-content">
                <span class="info-box-text"><?php echo $ac->fullname; ?> <span class="badge badge-warning"><?php echo $ac->registration; ?></span></span>
                <span class="info-box-number">Capacity: <?php echo $ac->maxcargo; ?> kgs</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
             <?php
             }
             ?>
            <!-- /.row -->
            </div>
          </div>
        </div>
        </div>
    </div>
  </div>
</section>