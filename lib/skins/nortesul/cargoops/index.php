<?php

$contabids = (is_array(FltbookData::getBidsForPilot(Auth::$pilot->pilotid)) 
? count(FltbookData::getBidsForPilot(Auth::$pilot->pilotid)) : 0 ) + (is_array(SchedulesData::getBids(Auth::$pilot->pilotid)) ? count(SchedulesData::getBids(Auth::$pilot->pilotid)) : 0 );

    if($contabids > 0){?>
        <script>window.location.replace("<?php echo SITE_URL;?>/schedules/bids");</script>
    <?php } else {
?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Cargo Operations</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv OPS&trade;</a></li>
                    <li class="breadcrumb-item active">Cargo Operations</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->
    <?php }?>