
            <div class="row">
                <div class="col-lg-12 col-md012 col-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Move Message</h3>
                        </div>
                        <div class="card-body">
                        <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Select Folder</label>
                            <select class="form-control" name="folder">
                                <option value="0">Inbox</option>
                                <?php 
                                    if(isset($folders)) {
                                        foreach ($folders as $folder) {
                                            echo '<option value="'.$folder->id.'">'.$folder->folder_title.'</option>';
                                        }
                                    }
                                ?>
                            </select>
                        </div>

                        <input type="hidden" name="mail_id" value="<?php echo $mail_id ?>" />
                        <input type="hidden" name="cur_folder" value="<?php echo $data->reciever_folder ?>" />
                        <input type="hidden" name="action" value="move" />
                        <input type="submit" class="btn btn-primary" value="Move" />
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .container-fluid -->
</section>
<!-- .content -->