         <div class="row">
            <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Reply Message</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <?php foreach($mail as $data) { ?>
                            <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="who_from" value="<?php echo Auth::$userinfo->pilotid ?>" />
                                <input type="hidden" name="who_to" value="<?php echo $data->who_from; ?>" />
                                <input type="hidden" name="oldmessage" value="<?php echo ' '.$data->thread_id.'<br /><br />'; ?>" />
                                <?php $user = PilotData::GetPilotData($data->who_from); $pilot = PilotData::GetPilotCode($user->code, $data->who_from); ?>

                                <div class="form-group">
                                    <label>Recipient</label>
                                    <input type="text" value="<?php echo $pilot; ?>" class="form-control" readonly>
                                </div>

                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text" class="form-control" name="subject" value="RE: <?php echo $data->subject;?>">
                                </div>

                                <div class="form-group">
                                    <label>Last Message</label>
                                    <blockquote style="height: 340px; overflow-y: auto;"><?php echo $data->message; ?></blockquote>
                                </div>

                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea name="message" class="summernote"></textarea>
                                </div>

                                <input type="hidden" name="action" value="send" />
                                <div class="col-6 float-right">
                                    <input type="submit" class="btn btn-primary btn-block" value="Reply">
                                </div>
                                <!-- /.col-6 -->
                            </form>
                        <?php } ?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->

<script>
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>