        <div class="col-lg-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Create Folder</h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Folder Name</label>
                            <input type="text" class="form-control" name="folder_title"/>
                        </div>
                        
                        <input type="hidden" name="action" value="savefolder" />
                        <input type="submit" class="btn btn-primary float-right" value="Create Folder" />
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->
<script>
    $('#createfolder').addClass('active');
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>