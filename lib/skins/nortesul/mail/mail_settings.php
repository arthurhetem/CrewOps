        <div class="col-lg-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Settings</h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Send email when new messages are received</label>
                            <select class="form-control" name="email">
                                <option value="0">No</option>
                                <option value="1"<?php if(MailData::send_email(Auth::$userinfo->pilotid) === TRUE){echo 'selected="seclected"';} ?>>Yes</option>
                            </select>
                        </div>

                        <input type="hidden" name="action" value="save_settings" />
                        <input type="submit" class="btn btn-primary float-right" value="Save" />
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->
<script>
    $('#settings').addClass('active');
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>