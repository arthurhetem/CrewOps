        <div class="col-lg-12 col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-title">Delete Folder</h5>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <?php 
                        $folders = MailData::checkforfolders(Auth::$userinfo->pilotid);
                        if(!$folders) {
                            echo '<div class="alert alert-primary">There are no folders to delete.</div>';
                        } else {
                    ?>
                        <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Select Folder</label>
                                <select class="form-control" name="folder_id">
                                    <?php foreach ($folders as $folder) {echo '<option value="'.$folder->id.'">'.$folder->folder_title.'</option>';}?>
                                </select>
                            </div>

                            <p class="alert alert-info"><h5><i class="icon fas fa-info"></i> Note</h5>All mails contained in the folder being deleted will be moved to the Inbox.</p>
                            
                            <input type="hidden" name="action" value="confirm_delete_folder" />
                            <input type="submit" class="btn btn-primary float-right" value="Delete Folder">
                        </form>
                    <?php } ?>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->
<script>
    $('#deletefolder').addClass('active');
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>