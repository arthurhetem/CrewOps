        <div class="row">
            <div class="col-12">
                <div class="card">
                <?php 
                            foreach ($mail as $data) {
                                if($data->who_to == Auth::$userinfo->pilotid){
                                    $user = PilotData::GetPilotData($data->who_from);
                                    $pilot = PilotData::GetPilotCode($user->code, $data->who_from);
                                }
                                if($data->who_from == Auth::$userinfo->pilotid){
                                    $user = PilotData::GetPilotData($data->who_to);
                                    $pilot = PilotData::GetPilotCode($user->code, $data->who_to);
                                }
                        ?>
                    <div class="card-header">
                        <h5 class="card-title">Read Message</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="mailbox-read-info">
                            <h5><?php echo $data->subject; ?></h5>
                            <h6>From: <?php echo $user->firstname.' '. $user->lastname.' '.$pilot; ?> <span class="mailbox-read-time float-right"><?php echo MailData::timeago($data->date); ?></span></h6>
                        </div>
                        <!-- /.mailbox-read-info -->
                        <div class="mailbox-read-message">
                            <?php echo nl2br($data->message); ?>
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-6">
                                <a href="<?php echo SITE_URL ?>/mail/move_message/<?php echo $data->id;?>" class="btn btn-primary btn-block">Move to Folder</a>
                            </div>
                            <div class="col-6">
                                <a href="<?php echo SITE_URL ?>/mail/reply/<?php echo $data->thread_id;?>" class="btn btn-primary btn-block">Reply</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-footer -->
                </div>
                <!-- /.card -->
                <?php } ?>
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- .container-fluid -->
</section>
<!-- .content -->
<script>
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>