
            <div class="row">
                <div class="col-lg-12 col-md012 col-12 col-sm-12">
                    <div class="card card-success card-outline">
                            <div class="card-header">
                                <h5 class="card-title">Sent Messages</h5>
                            </div>
                        <div class="card-body <?php if(!$mail){ echo ' '; } else { echo 'p-0'; }?>">
                            <?php
                            if(!$mail) {
                                echo '<div class="alert alert-primary">You have no sent messages.</div>';
                            } else {
                            ?>
                            <div class="table-responsive mailbox-mailbox-messages">
                                <table class="table table-hover">
                                    <tbody> 
                                    <?php
                                    foreach($mail as $thread) {
                                        if($thread->read_state=='0'){
                                            if($thread->deleted_state == '0') {
                                                $status = '<div class="badge badge-info">Unread</div>';
                                            } else {
                                                $status = '<div class="badge badge-danger">Unread & Deleted</div>';
                                            }
                                        } else {
                                            if($thread->deleted_state == '0') {
                                                $status = '<div class="badge badge-success">Read</div>';
                                            } else {
                                                $status = '<div class="badge badge-warning">Read & Deleted</div>';
                                            }
                                        }
        
                                        $user = PilotData::GetPilotData($thread->who_to); 
                                        $pilot = PilotData::GetPilotCode($user->code, $thread->who_to);
                                ?>
                                <tr>
                                    <td align="center">
                                        <?php echo $status; ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo SITE_URL ?>/index.php/Mail/item/<?php echo $thread->thread_id.'/'.$thread->who_to;?>"><?php echo $thread->subject; ?></a>
                                    </td>
                                    <td>
                                        <?php echo date('d/M H:ia', strtotime($thread->date)); ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo SITE_URL;?>/mail/sent_delete/?mailid=<?php echo $data->id;?>"><button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash text-red"></i></button></a>
                                    </td>
                                </tr>
                                <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php }?>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->

<script>
    $('#sent').addClass('active');
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>