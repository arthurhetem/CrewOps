<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<section class="content">
    <div class="row">
        <div class="col-lg-12 card">
            <div class="callout callout-info">
                <h4>Starting Download</h4>
                <p>Your download will be ready soon!. Click <a href="#" onclick="alerta()">here</a> to start.</p>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  function alerta(){
    swal('You will be redirected in 2 seconds!','Ok','success');
    var delay=2000; //1 second
    setTimeout(function() {
        window.location = "<?php echo $download->link;?>";
    }, delay);
};

    $('#resources').addClass('active');
    $('#downloads').addClass('active');
</script>