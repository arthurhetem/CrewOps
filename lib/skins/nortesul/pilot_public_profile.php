<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<?php
    if(!$pilot) {
        echo '
        <div class="section-header">
            <h1>Pilot Profile</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo SITE_URL; ?>">Dashboard</a></div>
<div class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></div>
<div class="breadcrumb-item">Pilot Profile</div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-primary">This pilot does not exist!</div>
    </div>
</div>';
return;
}
?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><b><?php echo $pilotcode;?></b>'s Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item">Pilots</li>
                    <li class="breadcrumb-item active"><?php echo $pilot->firstname . ' ' . $pilot->lastname;?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="card card-warning card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="<?php echo PilotData::getPilotAvatar($pilotcode); ?>" alt="User profile picture">
                        </div>
                        <h3 class="profile-username text-center"><?php echo $pilot->firstname .' '. $pilot->lastname; ?>
                        <img style="margin-left: 7px;"
                                        src="<?php echo Countries::getCountryImage($pilot->location); ?>"
                                        alt="<?php echo Countries::getCountryName($pilot->location); ?>" 
                                        title="<?php echo Countries::getCountryName($pilot->location);?>" data-toggle="tooltip"></a></h3>
                        <p class="text-muted text-center"><?php echo $pilotcode .' - '.$pilot->rank;?></p>
                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Flights</b> <a class="float-right"><?php echo $pilot->totalflights;?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Miles</b> <a
                                    class="float-right"><?php echo StatsData::TotalPilotMiles($pilotcode); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Hours</b> <a
                                    class="float-right"><?php echo Util::AddTime($pilot->totalhours, $pilot->transferhours); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Member Since</b> <a class="float-right"><?php echo date("d/M/Y", strtotime($pilot->joindate));?></a>
                            </li>
                        </ul>
                        <a href="#" class="btn btn-primary btn-block disabled" data-toggle="tooltip"
                            data-placement="top" title="Soon"><b>Add <i class="fas fa-plus-circle"></i></b></a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#flightmap" data-toggle="tab">Flight
                                    Map</a></li>
                            <li class="nav-item"><a class="nav-link" href="#latestflights" data-toggle="tab">Latest
                                    Flights</a></li>
                            <li class="nav-item"><a class="nav-link" href="#awards" data-toggle="tab">Awards</a></li>
                        </ul>
                    </div>
                    <div class="card-body p-0">
                        <div class="tab-content">
                            <div class="active tab-pane" id="flightmap">
                                <?php
                                    if(!$pireps) {
                                        echo "
                                            <div class='alert alert-primary mb-2' role='alert'>
                                                <strong>Opss!</strong> ".$pilot->firstname." don't have any flights!
                                            </div>
                                        ";
                                    } else {
                                        require 'flown_routes_map.php';
                                    }
                                ?>
                            </div>
                            <div class="tab-pane table-responsive" id="latestflights">
                                <?php
                                        if(!$pireps) {
                                            echo "
                                                <div class='alert alert-primary mb-2' role='alert'>
                                                    <strong>Opss!</strong> ".$pilot->firstname." don't have any flights!
                                                </div>
                                            ";
                                        } else {?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th scope="row">Flight Number</th>
                                            <th scope="row">Departure</th>
                                            <th scope="row">Arrival</th>
                                            <th scope="row">Aircraft</th>
                                            <th scope="row">Flight Time</th>
                                            <th scope="row">Submitted</th>
                                            <th scope="row">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                foreach ($pireps as $report) {
                            ?>
                                        <tr>
                                            <td><a
                                                    href="<?php echo url('/pireps/view/'.$report->pirepid);?>"><?php echo $report->code . $report->flightnum; ?></a>
                                            </td>
                                            <td><?php echo $report->depicao; ?></td>
                                            <td><?php echo $report->arricao; ?></td>
                                            <td><?php echo $report->aircraft . " ($report->registration)"; ?></td>
                                            <td><?php echo $report->flighttime; ?></td>
                                            <td><?php echo date(DATE_FORMAT, $report->submitdate); ?></td>
                                            <td>
                                                <?php
                                    if($report->accepted == PIREP_ACCEPTED)
                                        echo '<div id="success" class="badge badge-success">Accepted</div>';
                                    elseif($report->accepted == PIREP_REJECTED)
                                        echo '<div id="error" class="badge badge-danger">Rejected</div>';
                                    elseif($report->accepted == PIREP_PENDING)
                                        echo '<div id="error" class="badge badge-info">Approval Pending</div>';
                                    elseif($report->accepted == PIREP_INPROGRESS)
                                        echo '<div id="error" class="badge badge-warning">Flight in Progress</div>';
                                    ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php }?>
                            </div>
                            <div class="tab-pane" id="awards">
                                    <?php if(is_array($allawards)) { ?>
                                        <ul>
                                            <?php
                                            foreach($allawards as $award) {?>
                                                <img src="<?php echo $award->image?>" alt="<?php echo $award->descrip?>" />
                                            <li><?php echo $award->name ?></li>
                                            <?php } ?>
                                        </ul>
                                        <?php } else {?>
                                            <div class='alert alert-primary mb-2' role='alert'>
                                                    <strong>Opss!</strong> <?php echo $pilot->firstname; ?> don't have any awards!
                                                </div>
                                        <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$('#info').addClass('active');
    $('#pilots').addClass('active');
</script>