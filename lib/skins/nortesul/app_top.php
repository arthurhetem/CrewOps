<?php

$item = MailData::checkformail();
$items = $item->total;

$mailTop = MailData::getallmail(Auth::$userinfo->pilotid);
 ?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-envelope"></i>
          <span class="badge badge-<?php if(!$items) {echo 'success';} else {echo 'warning';}?> navbar-badge">
            <?php

                                        if(!$items) {
                                            echo '0';
                                        } else {
                                            echo $items;
                                        }
            ?>
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <?php
          if(!$mailTop) {
           echo '<span class="badge badge-success dropdown-header">You have no messages.</span>';
          } else {
            foreach($mailTop as $dataTop) { 
              $userMail = PilotData::GetPilotData($dataTop->who_from); 
              $pilotMail = PilotData::GetPilotCode($userMail->code, $dataTop->who_from); 
        ?>
              <a href="<?php echo SITE_URL ?>/index.php/Mail/item/<?php echo $dataTop->thread_id;?>" class="dropdown-item">
                <div class="media">
                  <img alt="User Avatar" src="<?php echo PilotData::getPilotAvatar($pilotMail); ?>" class="img-size-50 img-circle mr-3">
                  <div class="media-body">
                    <h3 class="dropdown-item-title"><?php echo "$userMail->firstname $userMail->lastname"; ?></h3>
                    <p class="text-sm"><?php echo $dataTop->subject; ?></p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> <?php echo MailData::timeago($dataTop->date); ?></p>
                  </div>
                </div>
              </a>
        <?php 
        if( !next($dataTop) ) {
          
      } else {?>
        <div class="dropdown-divider"></div>
      <?php } } } ?>
          <div class="dropdown-divider"></div>
          <a href="<?php echo SITE_URL; ?>/index.php/mail" class="dropdown-item dropdown-footer">See All Messages</a>
        </div>
      </li>
      <?php if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN)) { ?>
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-bell"></i>
          <?php
           $pendingpireps = (is_array(PIREPData::findPIREPS(array('p.accepted' => PIREP_PENDING))) ? count(PIREPData::findPIREPS(array('p.accepted' => PIREP_PENDING))) : 0);
           $pendingpilots = (is_array(PilotData::findPilots(array('p.confirmed' => PILOT_PENDING))) ? count(PilotData::findPilots(array('p.confirmed' => PILOT_PENDING))) : 0);

                                  $count = ($pendingpireps + $pendingpilots);?>
          <span class="badge badge-<?php if(!$count) {echo 'success';} else {echo 'danger';}?> navbar-badge">
            <?php

                                        if(!$count) {
                                            echo '0';
                                        } else {
                                            echo $count;
                                        }
                                      }
            ?>
          </span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right rounded-1rem">
          <span class="dropdown-header"><?php echo $count;?> Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="<?php echo SITE_URL?>/admin/index.php/pilotadmin/pendingpilots" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> <?php echo $pendingpilots ?></strong> Pending Applications
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo SITE_URL?>/admin/index.php/pirepadmin/viewpending" class="dropdown-item">
            <i class="fas fa-plane-arrival mr-2"></i> <strong><?php echo $pendingpireps ?></strong> Pending PIREPS
          </a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo SITE_URL?>/admin" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button"><i
            class="fas fa-th-large"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-warning elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo SITE_URL;?>" class="brand-link navbar-success">
      <img src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/img/logo_min.png" alt="ONEv Logo" class="brand-image img-circle elevation-3"
           style="opacity: .9">
      <span class="brand-text font-weight-bold">NorteSul</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo PilotData::getPilotAvatar(PilotData::getPilotCode(Auth::$userinfo->code, Auth::$userinfo->pilotid)); ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info" id="myprofile">
          <a href="<?php echo SITE_URL?>/index.php/profile" class="d-block"><?php echo Auth::$userinfo->firstname;?> <?php echo Auth::$userinfo->lastname;?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo SITE_URL;?>" class="nav-link" id="dashboard">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
              <a href="<?php echo SITE_URL;?>/mail" class="nav-link" id="mail">
                <i class="far fa-envelope nav-icon"></i>
                <p>NSmail&trade;</p>
              </a>
            </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="info">
              <i class="nav-icon fas fa-info-circle"></i>
              <p>
                NSv Info&trade;
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/career" class="nav-link" id="career">
                  <i class="far fa-address-card nav-icon"></i>
                  <p>Career</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/pilots" class="nav-link" id="pilots">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Pilots</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/airports" class="nav-link" id="airports">
                  <i class="fas fa-map-marker nav-icon"></i>
                  <p>Airports</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/fleet" class="nav-link" id="fleet">
                  <i class="fas fa-plane nav-icon"></i>
                  <p>Fleet</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="<?php echo SITE_URL;?>/toppilot" class="nav-link" id="leaderboard">
              <i class="nav-icon fas fa-trophy"></i>
              <p>
                Leaderboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo SITE_URL;?>/acars/livemap" class="nav-link" id="acars">
              <i class="nav-icon fas fa-map"></i>
              <p>
                Live Map
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo SITE_URL;?>/Screenshots" class="nav-link" id="screenshots">
              <i class="nav-icon fas fa-camera"></i>
              <p>
                Screenshot Center
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="ops">
              <i class="nav-icon fas fa-plane-departure"></i>
              <p>
                NSv OPS&trade;
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/pireps" class="nav-link" id="logbook">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Logbook</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/wthr" class="nav-link" id="weather">
                  <i class="fas fa-cloud nav-icon"></i>
                  <p>Weather</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/events" class="nav-link" id="events">
                  <i class="fas fa-calendar-alt nav-icon"></i>
                  <p>Events</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/Fltbook" class="nav-link" id="flops">
                  <i class="fas fa-paper-plane nav-icon"></i>
                  <p>Flight Operations</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/CargoOps" class="nav-link" id="cgops">
                  <i class="fas fa-box nav-icon"></i>
                  <p>Cargo Operations</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="social">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Social Medias
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="https://twitter.com/nortesulvirtual" class="nav-link">
                  <i class="fab fa-twitter nav-icon"></i>
                  <p>Twitter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="https://facebook.com/nortesul.va" class="nav-link">
                  <i class="fab fa-facebook nav-icon"></i>
                  <p>Facebook</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="https://www.youtube.com/channel/UCxeniklwjQp2FbNFwB5jlpw" class="nav-link">
                  <i class="fab fa-youtube nav-icon"></i>
                  <p>YouTube</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="https://www.instagram.com/nortesulva/" class="nav-link">
                  <i class="fab fa-instagram nav-icon"></i>
                  <p>Instagram</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="https://www.twitch.tv/nortesul" class="nav-link">
                  <i class="fab fa-twitch nav-icon"></i>
                  <p>Twitch</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link" id="resources">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                Resources
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo SITE_URL;?>/downloads" class="nav-link" id="downloads">
                  <i class="fas fa-download nav-icon"></i>
                  <p>Downloads</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="https://help.nortesulvirtual.com" class="nav-link">
                  <i class="fas fa-life-ring nav-icon"></i>
                  <p>HelpDesk</p>
                </a>
              </li>
            </ul>
          </li>
          <?php if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN)) { echo '
              <li class="nav-item">
              <a href="'.SITE_URL.'/admin" class="nav-link">
                <i class="fas fa-cog nav-icon"></i>
                <p>Administration</p>
              </a>
            </li>
            '; } ?>
            <li class="nav-item">
            <a href="<?php echo SITE_URL;?>/logout" class="nav-link">
              <i class="nav-icon fas fa-lock"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>