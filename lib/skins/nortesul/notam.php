<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<?php
$data = $postdate;
list($dia, $mes, $ano) = explode("/", $data);

switch ($mes) {
        case "01":    $mes = "Jan";    	break;
        case "02":    $mes = "Feb";   	break;
        case "03":    $mes = "Mar";       break;
        case "04":    $mes = "Apr";       break;
        case "05":    $mes = "May";       break;
        case "06":    $mes = "Jun";       break;
        case "07":    $mes =" Jul";       break;
        case "08":    $mes = "Aug";       break;
        case "09":    $mes = "Sep";       break;
        case "10":    $mes = "Oct";       break;
        case "11":    $mes = "Nov";       break;
        case "12":    $mes = "Dec";       break; 
 }
?>
<meta charset="utf-8"/>
<tr>
	<td><?php echo $dia ." ". $mes ." ". $ano;?></td>
	<td><a href="<?php echo SITE_URL;?>/index.php/notam/view/<?php echo $id;?>" class="text-muted"><?php echo $subject;?></a></td>
</tr>