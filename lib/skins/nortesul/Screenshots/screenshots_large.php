<?php
//simpilotgroup addon module for phpVMS virtual airline system
//
//simpilotgroup addon modules are licenced under the following license:
//Creative Commons Attribution Non-commercial Share Alike (by-nc-sa)
//To view full icense text visit http://creativecommons.org/licenses/by-nc-sa/3.0/
//
//@author David Clark (simpilot)
//@copyright Copyright (c) 2009-2010, David Clark
//@license http://creativecommons.org/licenses/by-nc-sa/3.0/

$pilot = PilotData::getPilotData($screenshot->pilot_id);
?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Screenshot Center</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item">Screenshot Center</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        <?php echo $pilot->firstname.' '.$pilot->lastname.' ('.PilotData::GetPilotCode($pilot->code, $pilot->pilotid); ?>)'s
                        Screenshot</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="widget-user-image mb-2  text-muted">
                                <img src="<?php echo PilotData::getPilotAvatar($pilot->code); ?>"
                                    alt="Sender Profile Picture" class="img-circle" style="height:40px; width: auto;">
                                <i class="fas fa-calendar"></i>
                                <?php echo date('d/m/Y', strtotime($screenshot->date_uploaded)); ?>
                            </div>
                            <p>
                                <?php echo $screenshot->file_description;?>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <img src="<?php echo SITE_URL; ?>/pics/<?php echo $screenshot->file_name; ?>"
                            alt="Flight Simulation Image" class="img-fluid">
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-6">
                            <?php
                            if(Auth::loggedin()){
                                $boost = ScreenshotsData::check_boost(Auth::$userinfo->pilotid, $screenshot->id);
                                if($boost->total > 0){
                                    echo 'Already Rated';
                                }else{
                            ?>
                            <form method="post" action="<?php echo SITE_URL ?>/index.php/Screenshots/addkarma">
                                <input type="hidden" name="id" value="<?php echo $screenshot->id; ?>" />
                                <button class="btn btn-sm btn-outline-primary" type="submit"><i
                                        class="fas fa-thumbs-up"></i></button>
                            </form>
                            <?php
                            }
                            }else {?>
                            <button class="btn btn-sm btn-primary"><i class="fas fa-thumbs-up"></i>
                                <?php echo $screenshot->rating; ?></button>
                            <?php }
                        ?>
                        </div>
                        <div class="col-6">
                            <p><i class="fas fa-eye text-primary"></i><?php echo $screenshot->views; ?></p>
                            <div class="pull-right">
                                <?php if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN))
                        { ?><a class="btn btn-outline-danger btn-sm"
                                    href="<?php echo SITE_URL ?>/index.php/Screenshots/delete_screenshot?id=<?php echo $screenshot->id; ?>">Delete
                                    Screenshot</a><?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="direct-chat-messages">
                        <?php 
                        if(!$comments) {
                            echo '<div class="badge badge-info col-md-12">There are no comments on this Screenshot.</div>';
                        } else {
                            foreach($comments as $comment) {
                                $pilot = PilotData::getPilotData($comment->pilot_id);?>
                        <div
                            class="direct-chat-msg <?php if ($comment->pilot_id == Auth::$pilot->pilotid) { echo 'right';}?>">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name float-left">
                                    <a
                                        href="<?php echo SITE_URL;?>/profile/view/<?php echo $comment->pilot_id;?>"><?php echo $pilot->firstname.' '.$pilot->lastname; ?></a>
                                </span>
                            </div>
                            <!-- /.direct-chat-infos -->
                            <img class="direct-chat-img img-bordered-sm"
                                src="<?php echo PilotData::getPilotAvatar($comment->pilot_id); ?>" alt="user image">
                            <div class="direct-chat-text">
                                <?php echo $comment->comment;?>
                            </div>
                        </div>
                        <?php }}?>
                    </div>
                </div>
                <div class="card-footer" style="display: block;">
                    <form action="<?php echo url('/Screenshots');?>" method="post" enctype="multipart/form-data">
                        <div class="input-group">
                            <input type="text" name="comment" placeholder="Type comment ..." class="form-control">
                            <input type="hidden" name="id" value="<?php echo $screenshot->id; ?>" />
                            <input type="hidden" name="action" value="add_comment" />
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-outline-primary">Add comment <i
                                        class="far fa-paper-plane"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#screenshots').addClass('active');
</script>