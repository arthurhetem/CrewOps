<?php
//simpilotgroup addon module for phpVMS virtual airline system
//
//simpilotgroup addon modules are licenced under the following license:
//Creative Commons Attribution Non-commercial Share Alike (by-nc-sa)
//To view full icense text visit http://creativecommons.org/licenses/by-nc-sa/3.0/
//
//@author David Clark (simpilot)
//@copyright Copyright (c) 2009-2010, David Clark
//@license http://creativecommons.org/licenses/by-nc-sa/3.0/


$pagination = new Pagination();
$pagination->setLink("screenshots?page=%s");
$pagination->setPage($page);
$pagination->setSize($size);
$pagination->setTotalRecords($total);
$screenshots = ScreenshotsData::getpagnated($pagination->getLimitSql());
?>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Screenshot Center</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
          <li class="breadcrumb-item">Screenshot Center</li>
        </ol>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="col-12 text-center">
            <?php if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN)){?>
            <a href="<?php echo SITE_URL ?>/screenshots/approval_list" class="btn btn-app">
              <i class="fas fa-cogs"></i> Screenshot Center Admin
            </a>
            <?php } ?>
            <a href="<?php echo SITE_URL ?>/screenshots/upload" class="btn btn-app">
              <i class="fas fa-paper-plane"></i> Send Screenshot
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <?php
    if (!$screenshots) { ?>
    <div class="col-12">
      <div class="alert alert-info">No screenshots have been uploaded yet, be the first!</div>
    </div>
  </div>
</div>
<script>
$('#screenshots').addClass('active');
</script>
    <?php 
      return;
      } 
      foreach($screenshots as $screenshot) {
        $pilot = PilotData::getpilotdata($screenshot->pilot_id);?>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12">
      <div class="card card-widget shadow-lg">
        <div class="widget-user-header text-white">
          <img src="<?php echo SITE_URL.'/pics/'.$screenshot->file_name; ?>" alt="Image" class="img-fluid">
        </div>
        <div class="card-footer">
          <div class="row">
            <div class="col-6"><?php echo $pilot->firstname.' '.$pilot->lastname; ?></div>
            <div class="col-6"><?php echo date("d/m/Y", strtotime($screenshot->date_uploaded)); ?></div>
          </div>
          <div class="row">
            <div class="col-6">
              <div class="btn-group">
                <a href="<?php echo SITE_URL; ?>/Screenshots/large_screenshot?id=<?php echo $screenshot->id; ?>"
                  class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="View Full Size"><i
                    class="fas fa-eye"></i></a>
                <a href="<?php echo SITE_URL; ?>/pics/<?php echo $screenshot->file_name; ?>"
                  class="btn btn-sm btn-primary" download data-toggle="tooltip" data-placement="top"
                  title="Download Image"><i class="fas fa-download"></i></a>
              </div>
            </div>
            <div class="col-3">
              <?php echo $screenshot->views; ?> <i class="fas fa-eye text-primary"></i>
            </div>
            <div class="col-3">
              <?php echo $screenshot->rating; ?> <i class="fas fa-thumbs-up text-primary"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php 
      }
      ?>
      <div class="col-12 text-center">
    <nav class="justify-content-center">
    <?php
      $navigation = $pagination->create_links();
      echo $navigation;
    ?>
    </nav>
    </div>
  </div>
</div>
<script>
$('#screenshots').addClass('active');
</script>