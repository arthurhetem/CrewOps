<?php
//simpilotgroup addon module for phpVMS virtual airline system
//
//simpilotgroup addon modules are licenced under the following license:
//Creative Commons Attribution Non-commercial Share Alike (by-nc-sa)
//To view full icense text visit http://creativecommons.org/licenses/by-nc-sa/3.0/
//
//@author David Clark (simpilot)
//@copyright Copyright (c) 2009-2010, David Clark
//@license http://creativecommons.org/licenses/by-nc-sa/3.0/
?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Screenshot Center</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item">Screenshot Center</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Send Screenshot</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo url('/Screenshots');?>" method="post" enctype="multipart/form-data">
                        <table class="profiletop">
                            <tr>
                                <td width="50%" valign="top">
                                    <h5>Terms and Conditions</h5>
                                    <ul>
                                        <li>Sending your screenshot, you give total permition to NorteSul use her</li>
                                        <li>Your Screenshot will be rejected in case that contain anything out of the
                                            theme Flight Simulation</li>
                                    </ul>
                                </td>
                                <td>

                                    <div class="form-group">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
                                        <label for="file">File to send:</label><br />
                                        <div class="custom-file">
                                            <input class="custom-file-input" id="file" type="file" name="uploadedfile" />
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label for="description">Insert a description to the Screenshot:</label><br />
                                    <textarea name="description" class="summernote"></textarea>
                                    </div>
                                    </p>

                                    <p>
                                        <input type="hidden" name="action" value="save_upload" />
                                        <button class="btn btn-outline-success btn-block" type="submit">Send <i class="fas fa-paper-plane    "></i></button>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <a href="<?php echo SITE_URL; ?>/Screenshots" class="btn btn-block btn-outline-primary"><i class="fas fa-arrow-left    "></i> Return</a>
    </div>
</div>
<script>
    $('#screenshots').addClass('active');
</script>