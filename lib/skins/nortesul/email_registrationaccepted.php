<!-- <?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?> -->
<html>

<head></head>

<body>
    <!DOCTYPE html
        PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Welcome Email</title>
        <meta name="viewport" content="width=device-width" />
        <style type="text/css">
            @media only screen and (max-width: 550px),
            screen and (max-device-width: 550px) {
                body[yahoo] .buttonwrapper {
                    background-color: transparent !important;
                }

                body[yahoo] .button {
                    padding: 0 !important;
                }

                body[yahoo] .button a {
                    background-color: #349ee5;
                    padding: 15px 25px !important;
                }
            }

            @media only screen and (min-device-width: 601px) {
                .content {
                    width: 600px !important;
                }

                .col387 {
                    width: 387px !important;
                }
            }
        </style>
    </head>

    <body bgcolor="#252d2f" style="margin: 0; padding: 0;" yahoo="fix">
        <!--[if (gte mso 9)|(IE)]>
        <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td>
        <![endif]-->
        <table align="center" border="0" cellpadding="0" cellspacing="0"
            style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
            <tr>
                <td align="center" bgcolor="#BABABA"
                    style="padding: 25px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 26px;">
                    <img src="https://nortesulvirtual.com/docs/img/emaillogo.png" alt="NorteSul Virtual" width="auto;"
                        height="120" style="display:block;" />
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#ffffff"
                    style="padding: 40px 20px 0 20px; color: #555555; font-family: Arial, sans-serif; font-size: 18px; line-height: 30px;">
                    <b>Dear <?php echo $pilot->firstname; ?>,</b>
                </td>
            </tr>
            <tr>
                <td align="left" bgcolor="#ffffff"
                    style="padding: 0 20px 40px 20px; color: #777777; font-family: Arial, sans-serif; font-size: 14px;  border-bottom: 1px solid #f6f6f6;">


                    Thank you for choosing us as your virtual airline partner. <br>
                    We're thrilled to have you onboard as the newest team member. We've built a world-class virtual
                    airline for you to enjoy and acquire knowledge. Our goal is to support, promote, and enjoy the
                    virtual skies.<br>
                    <br>

                    Our fleet and our destinations are designed to keep pilots constantly looking for the next
                    adventure.
                    <br><br>
                    Our staff is here to assist you if you have any questions, feel free to contact us anytime.
                    <br><br>
                    Below you can find your login details

                    <br><br>
                    Before you get started, let’s go over some of the great new features we have to offer you! <br>
                    - Dedicated support to make your journey as smooth as possible <br>
                    - smartCARS with automatic flight analysis after every flight <br>
                    - Built in simBrief dispatching <br>
                    - Accurate fuel planning and flight dispatching <br>
                    - Up-to-date schedules for the most realistic experience <br>
                    - Simulated aircraft maintenance checks <br>
                    - More online Events with VATSIM and POSCON <br>
                    - Hall of Fame awards....and so much more! <br>
                    <br>
                    These are just a very small portion of the amazing new features we have to offer you!
                    <br>
                    Below you will find your CrewOps log-in information. Once you’ve logged into the crew center, we
                    recommend taking a look at the Operations Manual which will help you get started with your first
                    flight, and then head over to the downloads page where you can download our smartCARS program,
                    which is used to track your flights.
                    <br>
                    <hr>
                    <b>Login ID : </b> <?php echo PilotData::GetPilotCode($pilot->code,$pilot->pilotid); ?><br>

                    <b>Password : </b> <i>Chosen during registration.</i><br>
                    <br>

                    If you have any questions or need help with anything, check out the FAQ section. If you still have
                    some questions, feel free to contact a member of the staff.
                    <br>
                    Again, thank you for choosing NorteSul Virtual, Captain. New horizons await you.
                    <br>
                    - Team NorteSul Virtual </td>
            </tr>

            <tr>
                <td align="center" bgcolor="#f9f9f9"
                    style="padding: 30px 20px 30px 20px; font-family: Arial, sans-serif;">
                    <table bgcolor="#349ee5" border="0" cellspacing="0" cellpadding="0" class="buttonwrapper">
                        <tr>
                            <td align="center" height="55"
                                style=" padding: 0 35px 0 35px; font-family: Arial, sans-serif; font-size: 22px;"
                                class="button">
                                <a href="https://crew.nortesulvirtual.com/login"
                                    style="color: #ffffff; text-align: center; text-decoration: none;">Login</a>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" bgcolor="#ffffff"
                    style="padding: 0 20px 40px 20px; color: #777777; font-family: Arial, sans-serif; font-size: 12px;  border-bottom: 1px solid #f6f6f6;">
                    <br>
                    If the button is not working, click <a
                        href="https://crew.nortesulvirtual.com/login">here</a>.
                    <i>We invite you to take part in active discussions on our Discord Server, <a
                            href="https://discord.gg/UbQje6P" target="_blank">https://discord.gg/UbQje6P</a>
                </td>
            </tr>
            <tr>
                <td align="center" bgcolor="#e9e9e9"
                    style="padding: 12px 10px 12px 10px; color: #888888; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;">
                    <b>NorteSul Virtual </b> <br>Powered by CrewCenter v1 &bull; Made by digimidia.dev

                </td>
            </tr>
            <tr>
                <td style="padding: 15px 10px 15px 10px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" width="100%"
                                style="color: #999999; font-family: Arial, sans-serif; font-size: 12px;">
                                https://crew.nortesulvirtual.com </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <!--[if (gte mso 9)|(IE)]>
                </td>
            </tr>
        </table>
        <![endif]-->
    </body>

    </html>
</body>

</html>