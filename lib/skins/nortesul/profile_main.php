<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<?php 
    $pilotCode = PilotData::getPilotCode(Auth::$userinfo->code, Auth::$userinfo->pilotid);
    $userinfo = Auth::$userinfo;

    $hrs = intval($userinfo->totalhours);
    $min = ($userinfo->totalhours - $hrs) * 100;

    $last_location = FltbookData::getLocation($userinfo->pilotid);
    $last_name = OperationsData::getAirportInfo($last_location->arricao);
    if(!$last_location) {
        FltbookData::updatePilotLocation($pilotid, Auth::$userinfo->hub);
    }

    // $percentage = ($pilot_hours/$nextrank->minhours) * 100;
    // $round = round($percentage);

    $pireps = PIREPData::getAllReportsForPilot($userinfo->pilotid);
    $pirep_list = PIREPData::getAllReportsForPilot(Auth::$pilot->pilotid);
?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">My Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item">Pilots</li>
                    <li class="breadcrumb-item active"><?php echo $pilot->firstname . ' ' . $pilot->lastname;?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <div class="card card-warning card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="<?php echo PilotData::getPilotAvatar($pilotCode); ?>" alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center">
                            <?php echo $userinfo->firstname.' '.$userinfo->lastname; ?> <img style="margin-left: 7px;"
                                src="<?php echo Countries::getCountryImage(Auth::$userinfo->location); ?>"
                                data-toggle="tooltip"
                                title="<?php echo Countries::getCountryName($userinfo->location); ?>"></h3>

                        <p class="text-muted text-center"><?php echo $userinfo->email; ?></p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Flights</b> <a class="float-right"><?php echo $userinfo->totalflights; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Miles</b> <a
                                    class="float-right"><?php echo StatsData::TotalPilotMiles($userinfo->pilotid); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Hours</b> <a class="float-right"><?php echo $hrs.'h '.round($min, 2).'m';?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Status</b> <a class="float-right"><?php
                                if($pilot->retired == 0) {
                                    echo '<span class="badge badge-success">Active</span>';
                                } elseif($spanilot->retired == 1) {
                                    echo '<span class="badge badge-danger">Inactive</span>';
                                } else {
                                    echo '<span class="badge badge-primary">On Leave</span>';
                                }
                            ?></a>
                            </li>
                        </ul>

                        <a href="<?php echo SITE_URL; ?>/index.php/profile/editprofile"
                            class="btn btn-primary btn-block"><b><i class="far fa-edit"></i> Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h4>Flights Map</h4>
                    </div>
                    <div class="card-body p-0">
                        <?php
                            if(!$pireps) {
                                echo "
                                <div class='card-body'>
                                    <div class='alert alert-primary mb-2' role='alert'>
                                        <strong>Opss!</strong> You don't have any flights!
                                    </div>
                                </div>
                                ";
                            } else {
                                require 'flown_routes_map.php';
                            }
                        ?>
                        <a data-toggle="modal" href="javascript::" data-target="#logbookModal"
                            class="btn btn-info btn-block"
                            style="border-top-left-radius: 0rem; border-top-right-radius: 0rem;">Pilot Logbook</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-passport mr-1"></i> Rank</strong>

                        <p class="text-muted">
                            <?php echo $pilot->rank; ?>
                        </p>

                        <hr>

                        <strong><i class="fas fa-map-marker-alt mr-1"></i> Current Location</strong>

                        <p class="badge badge-primary"><?php echo $last_name->icao; ?></p>

                        <hr>

                        <strong><i class="fas fa-plane mr-1"></i> Last PIREP</strong>

                        <p class="text-muted">
                            <?php
                                    $pireps = PIREPData::findPIREPS(array('p.pilotid' => Auth::$userinfo->pilotid));
                                    if(!$pireps) {
                                        echo 'NO PIREPs!';
                                    } else {
                                        echo date("d/M/Y", strtotime($userinfo->lastpirep));
                                    }?>
                        </p>

                        <hr>

                        <strong><i class="far fa-clock mr-1"></i> Joined</strong>

                        <p class="text-muted"><?php echo date("d/M/Y", strtotime($userinfo->joindate)); ?>
                    </div>
                    </p>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>