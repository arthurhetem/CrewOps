<html><head></head><body><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>Welcome Email</title>
                    <meta name="viewport" content="width=device-width" />
                    <style type="text/css">
                        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
                            body[yahoo] .buttonwrapper { background-color: transparent !important; }
                            body[yahoo] .button { padding: 0 !important; }
                            body[yahoo] .button a { background-color: #349ee5; padding: 15px 25px !important; }
                        }
            
                        @media only screen and (min-device-width: 601px) {
                            .content { width: 600px !important; }
                            .col387 { width: 387px !important; }
                        }
                    </style>
                </head>
                <body bgcolor="#252d2f" style="margin: 0; padding: 0;" yahoo="fix">
                    <!--[if (gte mso 9)|(IE)]>
                    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td>
                    <![endif]-->
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 100%; max-width: 600px;" class="content">
                        <tr>
                            <td align="center" bgcolor="#BABABA" style="padding: 25px 20px 20px 20px; color: #ffffff; font-family: Arial, sans-serif; font-size: 26px;">
                                <img src="https://nortesulvirtual.com/docs/img/emaillogo.png" alt="NorteSul Virtual" width="auto;" height="120" style="display:block;" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#ffffff" style="padding: 40px 20px 0 20px; color: #555555; font-family: Arial, sans-serif; font-size: 18px; line-height: 30px;">
                                <b>Hey there,</b>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" bgcolor="#ffffff" style="padding: 0 20px 40px 20px; color: #777777; font-family: Arial, sans-serif; font-size: 14px;  border-bottom: 1px solid #f6f6f6;">
                                Your password was reset, it is: <?php echo $newpw?>
                                You can login with this new password and change it.
                                <br><br>
                                <b>NorteSul Virtual</b>
                            </td>
                        </tr>
            
                        <tr>
                            <td align="center" bgcolor="#ffffff" style="padding: 0 20px 40px 20px; color: #777777; font-family: Arial, sans-serif; font-size: 12px;  border-bottom: 1px solid #f6f6f6;">
                                <br>
                                <i>This is an auto generated e-mail. For support, contact support@nortesulvirtual.com                </td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#e9e9e9" style="padding: 12px 10px 12px 10px; color: #888888; font-family: Arial, sans-serif; font-size: 12px; line-height: 18px;">
                                <b>NorteSul Virtual </b> <br>Powered by CrewOps v1 &bull; Made by digimidia.dev
            
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 15px 10px 15px 10px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td align="center" width="100%" style="color: #999999; font-family: Arial, sans-serif; font-size: 12px;">
                                            <!--www.flyingamericanvirtual.com-->
                                            https://crew.nortesulvirtual.com                      </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                            </td>
                        </tr>
                    </table>
                    <![endif]-->
                </body>
            </html>
            </body></html>