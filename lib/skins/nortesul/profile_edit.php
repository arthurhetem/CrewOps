<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">Settings</a></li>
                    <li class="breadcrumb-item active">Edit Profile</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary card-outline">
                    <div class="card-header">
                        <h5 class="card-title">Profile Info</h5>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo url('/profile');?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control"
                                    placeholder="<?php echo $userinfo->firstname.' '.$userinfo->lastname;?>" disabled>
                            </div>

                            <div class="form-group">
                                <label>Airline</label>
                                <input type="text" class="form-control" placeholder="<?php echo $userinfo->code?>"
                                    disabled>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email"
                                    value="<?php echo $userinfo->email;?>">
                                <?php
                            if(isset($email_error) && $email_error == true)
                                echo '<p class="text-danger">Please enter your email address</p>';
                        ?>
                            </div>

                            <div class="form-group">
                                <label>Location</label>
                                <select name="location" class="form-control">
                                    <?php
                                foreach($countries as $countryCode=>$countryName) {
                                    if($pilot->location == $countryCode)
                                        $sel = 'selected="selected"';
                                    else	
                                        $sel = '';
                                    
                                    echo '<option value="'.$countryCode.'" '.$sel.'>'.$countryName.'</option>';
                                }
                            ?>
                                </select>
                            </div>

                            <?php
                        if($customfields) {
                            foreach($customfields as $field) {
                                echo '<div class="form-group">';
                                echo '<label>'.$field->title.'</label>';
                                
                                if($field->type == 'dropdown') {
                                    $field_values = SettingsData::GetField($field->fieldid);				
                                    $values = explode(',', $field_values->value);
                                    
                                    
                                    echo "<select class='form-control' name=\"{$field->fieldname}\">";
                                
                                    if(is_array($values)) {		
                                        
                                        foreach($values as $val) {
                                            $val = trim($val);
                                            
                                            if($val == $field->value)
                                                $sel = " selected ";
                                            else
                                                $sel = '';
                                            
                                            echo "<option value=\"{$val}\" {$sel}>{$val}</option>";
                                        }
                                    }
                                    
                                    echo '</select>';
                                } elseif($field->type == 'textarea') {
                                    echo '<textarea name="'.$field->fieldname.'" class="form-control customfield_textarea">'.$field->value.'</textarea>';
                                } else {
                                    echo '<input type="text" class="form-control" name="'.$field->fieldname.'" value="'.$field->value.'" />';
                                }
                                
                                echo '</div>';
                            }
                        }
                    ?>

                            <div class="form-group">
                                <label>Avatar <span class="badge badge-info">Max resolution: 512x512px</span></label>
                                <input type="hidden" name="MAX_FILE_SIZE"
                                    value="<?php echo Config::Get('AVATAR_FILE_SIZE');?>" />
                                <input type="file" name="avatar" size="40" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Current Avatar</label>
                                <?php	
                            if(!file_exists(SITE_ROOT.AVATAR_PATH.'/'.$pilotcode.'.png')) {
                                echo '<br/>None selected';
                            } else {
                        ?>
                                <br />
                                <img src="<?php	echo SITE_URL.AVATAR_PATH.'/'.$pilotcode.'.png';?>" /></dd>
                                <?php } ?>
                            </div>

                            <input type="hidden" name="action" value="saveprofile" />
                            <input type="submit" name="submit" class="btn btn-primary float-right"
                                value="Save Changes" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-danger card-outline">
                    <div class="card-header">
                        <h5 class="card-title">Change Password</h5>
                    </div>
                    <div class="card-body">
                        <form action="<?php echo url('/profile');?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label>Actual Password</label>
                                <input type="password" class="form-control" name="oldpassword"
                                    placeholder="Actual Password">
                            </div>

                            <div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control" id="password" name="password1"
                                    placeholder="New Password" value="">
                            </div>

                            <div class="form-group">
                                <label>Confirm new Password</label>
                                <input type="password" class="form-control" name="password2"
                                    placeholder="Confirm Password" value="">
                            </div>

                            <input type="hidden" name="action" value="changepassword" />
                            <input type="submit" name="submit" class="btn btn-primary float-right"
                                value="Save Password" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>