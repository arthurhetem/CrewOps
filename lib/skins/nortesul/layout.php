<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>CrewOps - NorteSul</title>

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/css/adminlte.min.css">
  <!-- Pace Theme -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/css/pace-theme.css">
  <!-- Pulsating Icon on Maps -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/leaflet-icon-pulse/L.Icon.Pulse.css">
  <!-- Animate CDN -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
  <!-- Custom Theme -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/css/style.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Favicons -->
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/site.webmanifest">
  <link rel="mask-icon" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#603cba">
  <meta name="msapplication-TileImage" content="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/mstile-144x144.png">
  <meta name="msapplication-config" content="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">

  <!-- Leaflet -->
  <link rel="stylesheet" href="<?php echo SITE_URL?>/lib/js/leaflet/leaflet.css">
        <script src="<?php echo SITE_URL?>/lib/js/leaflet/leaflet.js"></script>
        <script src="<?php echo SITE_URL?>/lib/js/leaflet/leaflet-providers.js"></script>
        <script src="<?php echo SITE_URL?>/lib/js/leaflet/Leaflet.Geodesic.js"></script>
<!-- SweetAlert2 CDN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- SummerNote -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/summernote/summernote-bs4.min.css">
  <!-- flag-icon-css -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/flag-icon-css/css/flag-icon.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <?php echo $page_htmlhead; ?>
</head>
<?php
  global $themeClass;

if ($_COOKIE['theme'] === 'dark') {
  $themeClass = 'dark-theme';
} else {
  $themeClass = '';
}
?>
<body class="hold-transition <?php if(Auth::LoggedIn()){ echo 'sidebar-mini'; } else { echo 'login-page';}?> <?php echo $themeClass; ?>">
<?php if(Auth::LoggedIn()){ echo '<div class="wrapper">'; } else{ echo ' '; }?>
  <?php echo $page_htmlreq; ?>

  <?php
      // var_dump($_SERVER['REQUEST_URI']);
      # Hide the header if the page is not the registration or login page
      # Bit hacky, don't like doing it this way
      if (!isset($_SERVER['REQUEST_URI']) || ltrim($_SERVER['REQUEST_URI'],'/') !== SITE_URL.'/index.php/login' || ltrim($_SERVER['REQUEST_URI'],'/') !== SITE_URL.'/index.php/registration') {
        if(Auth::LoggedIn()) {
          Template::Show('app_top.php');
        }
      }
    ?>

  <!-- Content Wrapper. Contains page content -->
  <?php if(Auth::LoggedIn()){ echo '<div class="content-wrapper">'; } else{ echo ' ';}?>
    <?php echo $page_content; ?>
  </div>
  <!-- /.content-wrapper -->
  <?php
      // var_dump($_SERVER['REQUEST_URI']);
      # Hide the header if the page is not the registration or login page
      # Bit hacky, don't like doing it this way
      if (!isset($_SERVER['REQUEST_URI']) || ltrim($_SERVER['REQUEST_URI'],'/') !== SITE_URL.'/index.php/login' || ltrim($_SERVER['REQUEST_URI'],'/') !== SITE_URL.'/index.php/registration') {
        if(Auth::LoggedIn()) {
          Template::Show('app_bottom.php');
        }
      }
    ?>
  
<?php if(Auth::LoggedIn()){ echo '</div>'; }?>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/js/adminlte.min.js"></script>
<!-- Popper.js -->
<script src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/popper/umd/popper.min.js"></script>
<!-- PACE -->
<script src="<?php echo SITE_URL;?>/lib/skins/nortesul/assets/plugins/pace-progress/pace.min.js"></script>
<!-- FontAwesome Icons Kit -->
<script src="https://kit.fontawesome.com/277d69ce43.js" crossorigin="anonymous"></script>

<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/leaflet-ant-path.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/summernote/summernote-bs4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo SITE_URL?>/lib/skins/nortesul/assets/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<!-- Simbrief -->
<script type="text/javascript" src="<?php echo fileurl('lib/js/simbrief.apiv1.js');?>"></script>

<script>
  if(jQuery().summernote) {   
    $(".summernote").summernote({
       dialogsInBody: true,
      minHeight: 150,
    });
  }

  $(function () {
  $('[data-toggle="tooltip"]').tooltip({boundary: 'window'})
})

$("[name='my-checkbox']").bootstrapSwitch({
  onSwitchChange: function (e, state) {
    document.body.classList.toggle("dark-theme");

    // Let's say the theme is equal to light
  let theme = "light";
  // If the body contains the .dark-theme class...
  if (document.body.classList.contains("dark-theme")) {
    // ...then let's make the theme dark
    theme = "dark";
  }
  // Then save the choice in a cookie
  document.cookie = `theme=${theme};expires=2147483647`;
  }
});
</script>
</body>
</html>
