<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Pilots</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv Info&trade;</a></li>
                    <li class="breadcrumb-item active">Pilots</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <?php if(!$allpilots) { ?>
                    <div class="alert alert-danger"><h5><i class="icon fas fa-exclamation-triangle"></i> No Pilots Found</h5>This may be an error, contact our staff for more info.</div>
            <?php } ?>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Pilots List</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table" id="pilotsTable">
                            <thead>
                                <tr>
                                    <th>Pilot ID</th>
                                    <th>Name</th>
                                    <th>Rank</th>
                                    <th>HUB</th>
                                    <th>VATSIM</th>
                                    <th>POSCON</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($allpilots as $pilot) { ?>
                                <tr>
                                    <td width="1%" nowrap><a href="<?php echo url('/profile/view/'.$pilot->pilotid);?>">
                                            <?php echo PilotData::GetPilotCode($pilot->code, $pilot->pilotid)?></a>
                                    </td>
                                    <td>
                                    <span class="flag-icon flag-icon-<?php echo strtolower($pilot->location);?>"></span>
                                            
                                        <?php echo $pilot->firstname.' <strong>'.$pilot->lastname.'</strong>'; ?>
                                    </td>
                                    <td><img src="<?php echo $pilot->rankimage?>" class="img-fluid" style="width: 100px; height: auto;" alt="<?php echo $pilot->rank;?>" /></td>
                                    <td><?php echo $pilot->hub; ?></td>
                                    <td><?php
                                        $vatsimId = PilotData::GetFieldValue($pilot->pilotid, 'VATSIM');
                                        if($vatsimId < 1){
                                            echo "<span class='text-muted'>Not Linked</span>";
                                        } else {
                                            echo "<span class='text-green'>".$vatsimId."</span>";
                                        }?>
                                    </td>
                                    <td><?php
                                        $posconid = PilotData::GetFieldValue($pilot->pilotid, 'POSCON');
                                        if($posconid < 1){
                                            echo "<span class='text-muted'>Not Linked</span>";
                                        } else {
                                            echo "<span class='text-green'>".$posconid."</span>";
                                        }?>
                                    </td>
                                    <td>
                                        <?php
                                        if($pilot->retired == 0) {
                                            echo '<span class="badge badge-success">Active</span>';
                                        } elseif($pilot->retired == 1) {
                                            echo '<span class="badge badge-danger">Inactive</span>';
                                        } else {
                                            echo '<span class="badge badge-primary">On Leave</span>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#info').addClass('active');
    $('#pilots').addClass('active');
    $(window).load(function () {
    $('#pilotsTable').DataTable({
      "lengthChange": false,
      "ordering": true,
      "autoWidth": false,
      "responsive": true,
      "paging": false,
      "searching": false,
    });
  });
</script>