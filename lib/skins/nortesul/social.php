<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Social Networks</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="javascript::">NSv Com&trade;</a></li>
                <li class="breadcrumb-item active">Social Networks</li>
            </ol>
          </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->    
 
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Instagram Profile</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="//www.instagram.com/onev.brazil/" class="btn bg-navy btn-block btn-lg"><i class="fab fa-instagram"></i></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Facebook Page</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="//www.facebook.com/onev.brazil" class="btn bg-blue btn-block btn-lg"><i class="fab fa-facebook"></i></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Twitter Profile</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="//twitter.com/BrazilOnev" class="btn btn-primary btn-block btn-lg"><i class="fab fa-twitter"></i></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Youtube Channel</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <a href="//www.youtube.com/channel/UC_5incT9mQllIbAJC6lT20Q?view_as=subscriber" class="btn btn-danger btn-block btn-lg"><i class="fab fa-youtube"></i></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
	<!-- /.content -->
<script>
    $('#com').addClass('active');
    $('#social').addClass('active');
</script>