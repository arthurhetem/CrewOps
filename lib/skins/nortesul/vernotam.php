<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<?php
$data = $postdate;
list($dia, $mes, $ano) = explode("/", $data);

switch ($mes) {
        case "01":    $mes = "Jan";    	break;
        case "02":    $mes = "Feb";   	break;
        case "03":    $mes = "Mar";       break;
        case "04":    $mes = "Apr";       break;
        case "05":    $mes = "May";       break;
        case "06":    $mes = "Jun";       break;
        case "07":    $mes = "Jul";       break;
        case "08":    $mes = "Auug";       break;
        case "09":    $mes = "Sep";       break;
        case "10":    $mes = "Oct";       break;
        case "11":    $mes = "Nov";       break;
        case "12":    $mes = "Dec";       break; 
 }
?>
<meta charset="utf-8" />
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $subject;?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv OPS&trade;</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NOTAMs</a></li>
                    <li class="breadcrumb-item active"><?php echo $subject;?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-warning">
                    <div class="card-header">
                        <h3 class="card-title">Emited by <?php echo $postedby;?> at <?php echo $data;?></h3>
                    </div>
                    <div class="card-body">
                        <?php echo $body;?>
                    </div>
                </div>
                <a href="<?php echo SITE_URL;?>/index.php/notam">
                    <div class="btn btn-outline-primary btn-rounded">Go Back</div>
                </a>
            </div>
        </div>
    </div>
</div>