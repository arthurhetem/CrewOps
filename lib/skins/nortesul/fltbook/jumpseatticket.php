<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">Flight Operations</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="javascript::">NSv OPS&trade;</a></li>
          <li class="breadcrumb-item"><a href="javascript::">Flight Operations</a></li>
          <li class="breadcrumb-item active">Jumpseat</li>
        </ol>
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <form action="<?php echo url('/Fltbook/jumpseatPurchase');?>" method="post">
              <table class="table">
                <tr>
                  <td>Destination: <strong><?php echo $airport->name; ?></strong></td>
                </tr>
                <tr>
                  <td>Departure Date: <strong><?php echo date('m/d/Y') ?></strong></td>
                </tr>
                <tr>
                  <td>Total Cost: <strong>$<?php echo $cost; ?></strong></td>
                </tr>
              </table>
              <div style="text-align: center;">
                <a href="<?php echo url('/Fltbook');?>"><input type="button" class="btn btn-danger"
                    value="Cancel Jumpseat"></a>
                <input type="submit" class="btn btn-primary" value="Confirm Jumpseat">
              </div>
              <input type="hidden" name="arricao" value="<?php echo $airport->icao; ?>" />
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->