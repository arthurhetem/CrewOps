<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<?php

$contabids = (is_array(FltbookData::getBidsForPilot(Auth::$pilot->pilotid)) 
? count(FltbookData::getBidsForPilot(Auth::$pilot->pilotid)) : 0 ) + (is_array(SchedulesData::getBids(Auth::$pilot->pilotid)) ? count(SchedulesData::getBids(Auth::$pilot->pilotid)) : 0 );

    if($contabids > 0){?>
        <script>window.location.replace("<?php echo SITE_URL;?>/schedules/bids");</script>
    <?php } else {
?>

<?php
    $pilotid = Auth::$userinfo->pilotid;
    $last_location = FltbookData::getLocation($pilotid);
    $last_name = OperationsData::getAirportInfo($last_location->arricao);
    if(!$last_location) {
        FltbookData::updatePilotLocation($pilotid, Auth::$userinfo->hub);
    }
?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Flight Operations</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv OPS&trade;</a></li>
                    <li class="breadcrumb-item active">Flight Operations</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Flights Map</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <script src="https://rawgit.com/mapshakers/leaflet-icon-pulse/master/src/L.Icon.Pulse.js"></script>
                        <?php require 'search_form_script.php'; ?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Schedule Search</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form action="<?php echo url('/Fltbook');?>" method="post">
                            <div class="alert alert-info">
                                    <input id="depicao" name="depicao" type="hidden" value="<?php echo $last_location->arricao; ?>">
                                    Departing from <?php echo $last_name->name; ?> (<?php echo $last_name->icao; ?>)
                            </div>
                            <?php if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN)) {?>
                                        <div class="form-group row">
                                            <label for="airline" class="col-3 col-form-label">Airline:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" name="airline" id="airline">
                                                    <option value="" selected>Select airline</option>
                                                    <option value="NST">NorteSul Translados</option>
                                                    <option value="NSV">NorteSul</option>
                                                </select>
                                            </div>
                                        </div>
                                    <?php
                                    } else {?>
                                    <input id="airline" name="airline" type="hidden" value="NSV">
                                    <?php
                                    }
                                    ?>
                            <div class="form-group row">
                                <label for="aircraft" class="col-3 col-form-label">Aircraft:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="aircraft" id="aircraft">
                                        <option value="" selected>Select aircraft</option>
                                        <?php
                                            $airc = FltbookData::findAircraftsOnAirport($last_location->arricao);
                                            if(!$airc) {
                                                echo '<option>No Aircraft Available!</option>';
                                            } else {
                                                foreach ($airc as $air) {
                                                    $ai = FltbookData::getaircraftbyID($air->aircraft);
                                                    echo '<option value="'.$air->icao.'">'.$air->name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="arrival" class="col-3 col-form-label">Arrival Airfield:</label>
                                <div class="col-sm-9">
                                <select class="form-control" name="arricao" id="arrival">
                                    <option value="">Any</option>
                                    <?php
                                            $airs = FltbookData::arrivalairport($last_location->arricao);
                                            if(!$airs) {
                                                echo '<option>No Airports Available!</option>';
                                            } else {
                                                foreach ($airs as $air) {
                                                    $nam = OperationsData::getAirportInfo($air->arricao);
                                                    echo '<option value="'.$air->arricao.'">'.$air->arricao.' - '.$nam->name.'</option>';
                                                }
                                            }
                                    ?>
                                </select>
                                </div>
                            </div>
                            <input type="hidden" name="action" value="search" />
                            <input type="submit" name="submit" value="Search" class="btn btn-primary mr-1" style="float: right;">
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Roster Generator</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <form name="randomflights" id="randomflights" action="<?php echo SITE_URL?>/randomflights/getRandomFlights" method="post" class="form-horizontal">
                            <div class="alert alert-info">
                                    <input id="depicao" name="depicao" type="hidden" value="<?php echo $last_location->arricao; ?>">
                                    Departing from <?php echo $last_name->name; ?> (<?php echo $last_name->icao; ?>)
                                    <input id="airline" name="airline" type="hidden" value="ONE">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label" for="equipment">Aircraft:</label>
                                <div class="col-sm-10">
                                    <select id="equipment" name="equipment" class="form-control">
                                        <option value="">Select aircraft</option>
                                            <?php
                                                if(!$equipment) $equipment = array();
                                                foreach($equipment as $equip)
                                                {
                                                        echo '<option value="'.$equip->icao.'">'.$equip->name.'</option>';
                                                }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label" for="count">Rosters:</label>
                                <div class="col-sm-10">
                                    <select id="count" name="count" class="form-control">
                                        <option value="2">2</option>
                                        <option value="4">4</option>
                                        <option value="6">6</option>
                                        <option value="8">8</option>
                                        <option value="10">10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="duration">Duration of each leg:</label>
                                <input type="range" class="custom-range custom-range-warning" id="duration" name="duration" onchange="updateRangeValue(this.value)" min="1" max="12">
                            </div>
                            <span class="badge text-sm badge-success" id="mostratempo"></span>
                            <input type="submit" name="submit" value="Generate" class="btn btn-primary float-right">
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Jumpseat</h5>
                    </div>
                    <div class="card-body">
                    <form action="<?php echo url('/Fltbook/jumpseat');?>" method="post">
                    <div class="form-group">
                        <label>Destination</label>
                        <select class="form-control" onchange="calculate_transfer(this.value)" name="depicao">
                            <option value="" selected disabled>Select an option</option>
                            <?php
                                foreach($airports as $airport) {
                                    if($airport->icao == $last_location->arricao) {
                                        continue;
                                    }

                                    echo '<option value="'.$airport->icao.'">'.$airport->icao.' - '.$airport->name.'</option>';
                                }
                            ?>
                        </select>
                        <div style="margin-top: 5px;" id="errors"></div>
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Cost</label>
                                <input type="text" class="form-control" id="jump_purchase_cost" readonly>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Distance</label>
                                <input type="text" class="form-control" id="distance_travelling" readonly>
                            </div>
                        </div>
                    </div>

                    <input type="submit" id="purchase_button" disabled="disabled" class="btn btn-primary mr-1" style="float: right;" value="Purchase Transfer">
                    <input type="hidden" name="cost">
                    <input type="hidden" name="airport">
                </form>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->

<script>
    $('#ops').addClass('active');
    $('#flops').addClass('active');

    function updateRangeValue(val) {
          document.getElementById('mostratempo').innerHTML=`${val} Hours`;
        }
    
        function calculate_transfer(arricao) {
        var distancediv = $('#distance_travelling')[0];
        var costdiv     = $('#jump_purchase_cost')[0];
        var errorsdiv     = $('#errors')[0];
        errorsdiv.innerHTML = '';
        $.ajax({
            url: baseurl + "/action.php/Fltbook/get_jumpseat_cost",
            type: 'POST',
            data: { depicao: "<?php echo $last_location->arricao; ?>", arricao: arricao, pilotid: "<?php echo Auth::$userinfo->pilotid; ?>" },
            success: function(data) {
            data = $.parseJSON(data);
            console.log(data);
            if(data.error) {
                $("#purchase_button").prop('disabled', true);
                errorsdiv.innerHTML = '<span class="badge badge-danger">Not enough funds for this transfer!</font>';
            } else {
                $("#purchase_button").prop('disabled', false);
                distancediv.value = data.distance + "nm";
                costdiv.value = "$" + data.total_cost;
            }
            },
            error: function(e) {
            console.log(e);
            }
        });
    }
</script>

<?php }?>