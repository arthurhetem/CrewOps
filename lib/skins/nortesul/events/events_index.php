<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Events</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">NSv OPS&trade;</a></li>
                    <li class="breadcrumb-item active">Events</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Upcoming Events</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" id="corpo">
                    <?php
                    if(!$events) {
                        echo '<div class="alert alert-info"><h5><i class="fas fa-info-circle"></i> Oops</h5>There are no upcoming events, sorry.</div>';
                    } else {
                ?>
                <script>$("#corpo").addClass("p-0");</script>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Event</th>
                            <th scope="col">Details/Signups</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($events as $event) {
                                if($event->active == '2') {
                                    continue;
                                }

                                echo '<tr><td>'.date('d/m/Y', strtotime($event->date)).'</td>';
                                echo '<td>'.$event->title.'</td>';
                                echo '<td><a href="'.SITE_URL.'/index.php/events/get_event?id='.$event->id.'">Details/Signups</a></td></tr>';
                            }
                        ?>
                    </tbody>
                </table>
                <?php } ?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Past Events</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" id="corpo2">
                    <?php
                    if(!$history) {
                        echo '<div class="alert alert-info"><h5><i class="fas fa-info-circle"></i> Oops</h5>There are no past events.</div>';
                    } else {
                ?>
                <script>$("#corpo2").addClass("p-0");</script>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Event</th>
                            <th scope="col">Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach($history as $event) {
                                echo '<tr><td>'.date('d/m/Y', strtotime($event->date)).'</td>';
                                echo '<td>'.$event->title.'</td>';
                                echo '<td><a href="'.SITE_URL.'/index.php/events/get_past_event?id='.$event->id.'">Details</a></td></tr>';
                            }
                        ?>
                    </tbody>
                </table>
                <?php } ?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#ops').addClass('active');
    $('#events').addClass('active');
</script>