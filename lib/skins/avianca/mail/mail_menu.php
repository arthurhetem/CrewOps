<?php


    $item = MailData::checkformail();
    $items = $item->total;
?>


<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">ONEmail&trade;</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="javascript::">ONEv Com&trade;</a></li>
                <li class="breadcrumb-item active">ONEmail&trade;</li>
            </ol>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
	<!-- /.content-header -->


    
<!-- Main content -->
<section class="content">
      <div class="container-fluid">
          <div class="row">
              <div class="col-12">
                  <div class="card">
                      <div class="card-body">
                          <div class="row">
                            <div class="col-sm-8">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo SITE_URL ?>/mail" id="inbox">Inbox 
                                        <span class="badge <?php if ($items > 0) {echo 'badge-info'; } else { echo 'badge-success'; }?>">
                                        <?php echo $items; ?></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo SITE_URL ?>/mail/newfolder" id="createfolder">Create Folder</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo SITE_URL ?>/mail/deletefolder" id="deletefolder">Delete Folder</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo SITE_URL ?>/mail/settings" id="settings">Settings</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo SITE_URL ?>/mail/sent" id="sent">Sent Messages</a>
                                    </li>
                                    <li class="nav-item ml-2">
                                        <a href="javascript::" data-toggle="modal" data-target="#composeForm" class="nav-link icon-left bg-gradient-orange text-white"><i class="far fa-edit"></i>Compose Message</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.col-sm-6 -->
                            <div class="col-sm-4">
                                <ul class="nav nav-pills float-right">
                                    <?php
                                        if (isset($folders)) {
                                            foreach ($folders as $folder) {
                                                echo '<li style="margin-right: 10px;" class="nav-item"> <a class="nav-link active" href="'.SITE_URL.'/index.php/Mail/getfolder/'.$folder->id.'">'.$folder->folder_title.'</a> </li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                            <!-- /.col-sm-6 -->
                          </div>
                      </div>
                  </div>
              </div>
          </div>