
            <div class="row">
                <div class="col-lg-12 col-md012 col-12 col-sm-12">
                    <div class="card card-primary card-outline">
                        <?php if(isset($folder)){?>
                            <div class="card-header">
                                <h5 class="card-title"><?php echo $folder->folder_title; ?> Folder</h5>

                                <div class="card-tools">
                                <a href="<?php echo SITE_URL.'/mail/editfolder/'.$folder->id; ?>" type="button" class="btn btn-tool"><i class="fas fa-minus"></i></a>
                                </div>
                            </div>
                        <?php }?>
                        <div class="card-body <?php if(!$mail){ echo ' '; } else { echo 'p-0'; }?>">
                            <?php
                            if(!$mail) {
                                echo '<div class="alert alert-primary">You have no messages.</div>';
                            } else {
                            ?>
                            <div class="table-responsive mailbox-messages">
                                <table class="table table-hover">
                                    <tbody> 
                                        <?php
                                            foreach($mail as $data) {
                                                if ($data->read_state=='0') {
                                                    $status = '<i class="fas fa-envelope"></i>' ;
                                                } else {
                                                    $status = '<i class="fas fa-envelope-open"></i>';
                                                }
                
                                                $user = PilotData::GetPilotData($data->who_from); 
                                                $pilot = PilotData::GetPilotCode($user->code, $data->who_from);
                                        ?>
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="<?php echo SITE_URL;?>/mail/delete/<?php echo $data->id;?>"><button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash text-red"></i></button></a>
                                                    <a href="<?php echo SITE_URL;?>/mail/reply/<?php echo $data->thread_id; ?>"><button type="button" class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Reply"><i class="fas fa-reply text-green"></i></button></a>
                                                </div>
                                            </td>
                                            <td class="mailbox-name">
                                                <a href="<?php echo SITE_URL ?>/mail/item/<?php echo $data->thread_id;?>"><img alt="image" src="<?php echo PilotData::getPilotAvatar($pilot); ?>" class="rounded-circle" width="35"> <span class="ml-3"><?php echo "$user->firstname $user->lastname"; ?></span></a>
                                            </td>
                                            <td class="mailbox-subject">
                                               <b><?php echo $data->subject; ?></b> - <small class="text-muted"><?php echo substr($data->message, 0, 5);?>...</small>
                                            </td>
                                            <td class="mailbox-date">
                                                <?php echo date('d/M H:i', strtotime($data->date)); ?>
                                            </td>
                                            <td class="mailbox-star">
                                                <?php echo $status; ?>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php }?>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <!-- .container-fluid -->
</section>
<!-- .content -->

<script>
    $('#inbox').addClass('active');
    $('#com').addClass('active');
    $('#mail').addClass('active');
</script>