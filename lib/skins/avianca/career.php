<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Career</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv Info&trade;</a></li>
                    <li class="breadcrumb-item active">Career</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-id-badge"></i> Pilot Ranks</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="row">Rank Title</th>
                                        <th scope="row">Minimum Hours</th>
                                        <th scope="row">Pay Rate/Hour</th>
                                        <th scope="row">Can Fly</th>
                                        <th scope="row">Rank Image</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($ranks as $rank) { ?>
                                    <tr>
                                        <td><?php echo $rank->rank; ?></td>
                                        <td><?php echo $rank->minhours; ?></td>
                                        <td>$<?php echo $rank->payrate; ?>/hr</td>
                                        <td> 
                                            <?php $rankai = CareerData::getaircrafts($rank->rankid); 
                                            if(!$rankai) {echo '<span class="badge badge-success">All Aircrafts <i class="fas fa-check"></i></span>';}
                                            else {
                                                $i = 0;
                                                foreach($rankai as $ran) {
                                                    $i++;
                                                    if($i > 1) echo ', ';
                                                    echo $ran->icao;
                                                } 
                                            } ?></td>
                                        <td><img src="<?php echo $rank->rankimage; ?>" title="<?php echo $rank->rank; ?>" /></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-award"></i> Awards</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="row">Award</th>
                                        <th scope="row">Description</th>
                                        <th scope="row"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!$generaward) {
                                        echo '<tr><td align="center" colspan="3"><span class="badge badge-info">There are no awards at this time!</span></td></tr>';
                                    } else {
                                        foreach($generaward as $gen) { ?>
                                    <tr>
                                        <td><?php echo $gen->name; ?></td>
                                        <td><?php echo $gen->descrip; ?></td>
                                        <td><img src="<?php echo $gen->image; ?>" title="<?php echo $gen->name; ?>" /></td>
                                    </tr>
                                    <?php } } ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-md-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#info').addClass('active');
    $('#career').addClass('active');
</script>