<div class="login-box animate__animated animate__fadeInRight">
  <div class="card">
    <div class="card-body login-card-body">
        <div class="login-logo">
            <img src="<?php echo SITE_URL;?>/lib/skins/avianca/assets/img/logo.svg" alt="" style=" width: auto; height: 150px;">
        </div>
        <!-- /.login-logo -->
        
      <form action="<?php echo url('/login');?>" method="post">
        <div class="input-group mb-4">
          <input type="text" class="form-control" name="email" placeholder="Email or ONEv ID">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-danger">
              <input type="checkbox" name="remember" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <input type="hidden" name="redir" value="" />
		        <input type="hidden" name="action" value="login" />
            <button type="submit" class="btn btn-primary btn-block"><i class="fas fa-sign-in-alt    "></i> Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <p class="mt-5">
        <a href="forgot-password.html" class="text-muted"><i class="fas fa-exclamation-triangle    "></i> Forgot Password</a>
      </p>
      <div class="col-xs-12 text-center mt-10">
                    <small>© <?php echo date("Y");?> ONEv Inc. is not affiliated with, nor sponsored by Avianca Holdings. All rights reserved.</small>
                </div>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->