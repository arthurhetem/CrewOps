<?php
	$userinfo = Auth::$userinfo;

	$hrs = intval($userinfo->totalhours);
	$min = ($userinfo->totalhours - $hrs) * 100;

	$touchstats = TouchdownStatsData::pilot_average($userinfo->pilotid);

	$events = EventsData::get_upcoming_events();
?>
<?php if(Auth::LoggedIn()) { ?>
<!-- Content Header (Page header) -->
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
	<!-- /.content-header -->
	
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-user"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">PIREPs filed</span>
                <span class="info-box-number">
				<?php echo $userinfo->totalflights; ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-star"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">FriendPoints&trade; earned</span>
                <span class="info-box-number"><?php echo $userinfo->totalpay; ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-clock"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Time Flown</span>
                <span class="info-box-number"><?php echo $hrs.'h '.round($min, 2).'m';?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-plane-arrival"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Landing Average</span>
                <span class="info-box-number"><?php echo substr($touchstats, 0, 4); ?> FPM</span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
        </div>
		<!-- /.row -->
		
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
				<h5 class="card-title">Live Map</h5>
              </div>
              <!-- /.card-header -->
              <script src="https://rawgit.com/mapshakers/leaflet-icon-pulse/master/src/L.Icon.Pulse.js"></script>
              <div class="card-body p-0">
			  	<?php require 'acarsmap.php'; ?>
              </div>
              <!-- ./card-body -->
            </div>
            <!-- /.card -->
          </div>
		  <!-- /.col -->
        </div>
        <!-- /.row -->
		<?php
			$lastbids = SchedulesData::GetAllBids();
			$countBids = (is_array($lastbids) ? count($lastbids) : 0);
		?>
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-8">
            <!-- MAP & BOX PANE -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Upcoming Departures</h3>

                <div class="card-tools">
					<?php if(!$countBids) { ?>
						<span class="badge badge-info">No Departures</span>
						<?php } else { ?>
						<span class="badge badge-success">Upcoming</span>
					<?php } ?>
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body <?php if(!$countBids){ } else { echo 'p-0'; }?>">
                <div class="d-md-flex">
					<?php if(!$countBids) { ?>
						<div class="alert alert-danger col-12">
							<div class="alert-title">Oops</div>
								Looks like there are no upcoming departures at the moment, do you feel like flying? Click <a href="<?php echo SITE_URL?>/index.php/fltbook">here</a> to bid a flight!
						</div>
				<?php } else { ?>
					<table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>
                                <div align="center">Flight #</div>
                            </th>
                            <th>
                                <div align="center">Pilot</div>
                            </th>
                            <th>
                                <div align="center">Slot added on</div>
                            </th>
                            <th>
                                <div align="center">Slot Expires on</div>
                            </th>
                            <th>
                                <div align="center">Departure</div>
                            </th>
                            <th>
                                <div align="center">Arrival</div>
                            </th>
                            <th>
                                <div align="center">Registration</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
							foreach($lastbids as $lastbid) {
								$flightid = $lastbid->id
						?>
						<tr>
							<td height="25" width="10%" align="center"><span><?php echo $lastbid->code; ?><?php echo $lastbid->flightnum; ?></span></td>
							<?php
								$params = $lastbid->pilotid;

								$pilot = PilotData::GetPilotData($params);
								$pname = $pilot->firstname;
								$psurname = $pilot->lastname;
								$now = strtotime(date('d-m-Y',strtotime($lastbid->dateadded)));
								$date = date("d-m-Y", strtotime('+48 hours', $now));
							?>
							<td height="25" width="10%" align="center"><span><?php echo '<a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Click to view Pilot Information!" href="  '.SITE_URL.'/index.php/profile/view/'.$pilot->pilotid.'">'.$pname.'</a>';?></span></td>
							<td height="25" width="10%" align="center"><span class="text-success"><?php echo date('d-m-Y',strtotime($lastbid->dateadded)); ?></span></td>
							<td height="25" width="10%" align="center"><span class="text-danger"><?php echo $date; ?></span></td>
							<td height="25" width="10%" align="center"><span><?php echo '<a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Click to view Airport Information!" href="  '.SITE_URL.'/index.php/airports/get_airport?icao='.$lastbid->depicao.'">'.$lastbid->depicao.'</a>';?></span></td>
							<td height="25" width="10%" align="center"><span><?php echo '<a class="btn btn-default btn-sm" data-toggle="tooltip" data-placement="top" title="Click to view Airport Information!" href="'.SITE_URL.'/index.php/airports/get_airport?icao='.$lastbid->arricao.'">'.$lastbid->arricao.'</a>';?></span></td>
							<td height="25" width="10%" align="center"><span><a class="btn btn- btn-sm" data-toggle="tooltip" data-placement="top" title="Click to view Aircraft Information!" href="<?php echo SITE_URL?>/index.php/Fleet/view/<?php echo '' . $lastbid->id . ''; ?>"><?php echo $lastbid->aircraft; ?></a></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
				<?php } ?>
                </div><!-- /.d-md-flex -->
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="row">
              <div class="col-md-6">
                <!-- DIRECT CHAT -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">Airline NOTAMs</h3>

                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
				  	<?php MainController::Run('News', 'ShowNewsFront', 1); ?>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!--/.direct-chat -->
              </div>
              <!-- /.col -->

              <div class="col">
                <!-- USERS LIST -->
                <div class="card">
                  <div class="card-body p-0">
				  <iframe src="//discordapp.com/widget?id=609222033133404170&theme=dark" width="100%" height="500" allowtransparency="true" frameborder="0"></iframe>
			</div>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!--/.card -->
              </div>
              <!-- /.col -->
            </div>
			<!-- /.row -->
			<div class="col-md-4">
            <!-- Info Boxes Style 2 -->
            <div class="info-box mb-3 bg-warning">
              <span class="info-box-icon"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pilots</span>
                <span class="info-box-number"><?php echo StatsData::PilotCount(); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-success">
              <span class="info-box-icon"><i class="far fa-paper-plane"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Flights</span>
                <span class="info-box-number"><?php echo StatsData::TotalFlights(); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-danger">
              <span class="info-box-icon"><i class="fas fa-map"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Routes</span>
                <span class="info-box-number"><?php echo StatsData::TotalSchedules (); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-info">
              <span class="info-box-icon"><i class="far fa-hourglass"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Hours</span>
                <span class="info-box-number"><?php echo StatsData::TotalHours(); ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <!-- /.card -->
          </div>
          <!-- /.col -->
          </div>
		  <!-- /.col -->

		<?php
			if($event) {
			foreach($events as $event) {
		?>
			<div class="row">
				<!-- Left col -->
				<div class="col-md-8">
					<!-- EVENTS -->
					<div class="card">
					<div class="card-header">
						<h3 class="card-title">Upcoming Events</h3>

						<div class="card-tools">
						<a href="<?php echo SITE_URL.'/events/get_event?id='.$event->id; ?>" class="btn btn-info"><?php echo $event->title; ?></a>

						<button type="button" class="btn btn-tool" data-card-widget="collapse">
							<i class="fas fa-minus"></i>
						</button>
						<button type="button" class="btn btn-tool" data-card-widget="remove">
							<i class="fas fa-times"></i>
						</button>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body p-0">
					<a href="<?php echo SITE_URL.'/index.php/events/get_event?id='.$event->id; ?>">
							<img class="img-fluid" src="<?php echo $event->image; ?>" alt="<?php echo $event->title; ?>">
						</a>
					</div>
					<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		<?php break; } } ?>
      </div>
</section>
	<!-- /.content -->
<script>
	$('#dashboard').addClass('active');
</script>
<?php } else {
		header('Location:'.SITE_URL.'/login');
	} ?>