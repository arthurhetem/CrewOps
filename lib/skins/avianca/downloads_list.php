<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Downloads</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="javascript::">Resources</a></li>
                <li class="breadcrumb-item active">Downloads</li>
            </ol>
          </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Essentials for Flight Operations</h5>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                    <a href="//tfdidesign.com/smartcars/app.php?action=download&airlineid=872&language=en-US">
                                        <div class="downloads-box levanta">
                                            <h4 class="text-center text-red"><i class="far fa-compass fa-4x fa-spin"></i></h4>
                                            <h4 class="text-center text-red">SmartCARS 2.0</h4>
                                            <h4 class="text-center text-muted"><smalL class="text-black">Flight register software developed by TFDi Design (Required for flight register).</small></h4>
                                        </div>
                                        <!-- /.downloads-box -->
                                    </a>
                            </div>
                            <!-- /.col-6 -->
                            <div class="col-6">
                                    <a href="//tfdidesign.com/dl.php?type=d&id=15">
                                        <div class="downloads-box levanta">
                                            <h4 class="text-center text-red"><i class="far fa-file fa-4x"></i></h4>
                                            <h4 class="text-center text-red">SmartCARS User Manual</h4>
                                            <h4 class="text-center text-muted"><smalL class="text-black">smartCARS is a modern and easy to use virtual flight logging software designed and developed by TFDi Design. Download this manual to learn more.</small></h4>
                                        </div>
                                        <!-- /.downloads-box -->
                                    </a>
                            </div>
                            <!-- /.col-6 -->
                       </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col-6 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
	<!-- /.content -->
<script>
    $('#resources').addClass('active');
    $('#downloads').addClass('active');
</script>