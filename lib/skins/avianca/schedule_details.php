<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Flight Details <span class="badge badge-info"><?php echo $schedule->code.$schedule->flightnum; ?></span></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv OPS&trade;</a></li>
                    <li class="breadcrumb-item active">Flight Details</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Schedule Timing</h3>
                </div>
                <div class="card-body">
                        <p>Departure time: <b><?php echo $schedule->deptime ;?></b> Local time (<?php echo $schedule->depicao;?>)</p>
                        <p>Arrival time: <b><?php echo $schedule->arrtime ;?></b> Local time (<?php echo $schedule->arricao;?>)</p>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Airport Information Services</h3>
                </div>
                <div class="card-body">
                        <a href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $schedule->depicao; ?>" class="btn btn-primary btn-block"><?php echo $schedule->depicao; ?></a>
                        <a href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $schedule->arricao; ?>" class="btn btn-secondary btn-block"><?php echo $schedule->arricao; ?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-gradient-dark">
                <div class="card-header">
                    <h3 class="card-title">PIREP Route</h3>
                </div>
                <div class="card-body">
                    <script src="https://rawgit.com/mapshakers/leaflet-icon-pulse/master/src/L.Icon.Pulse.js"></script>
                    <?php require 'schedule_map.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>