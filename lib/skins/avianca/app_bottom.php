<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
        <h5>Customization</h5>
        <p>Night Mode</p>
        <input type="checkbox" name="my-checkbox" data-switch-set="false" <?php if ($_COOKIE['theme'] === 'dark') { echo 'checked'; } ?>>
    </div>
</aside>
<!-- /.control-sidebar -->

<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Made with <i class="fas fa-heart text-danger"></i> by digimidia.dev <small class="badge bg-indigo">Version
            0.6.1</small>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020 - <?php echo date("Y");?> ONEv Inc. is not affiliated with, nor sponsored by Avianca
        Holdings.</strong>
</footer>

<!-- Fltbook Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

<!-- Profile Logbook Modal -->
<div class="modal fade" id="logbookModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="logbookLabel1"><?php echo Auth::$userinfo->firstname.' '.Auth::$userinfo->lastname; ?>'s
             Logbook</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body p-0">
                <?php
                                $pireps = PIREPData::getAllReportsForPilot($userinfo->pilotid);
                                if(!$pireps) {
                                    echo '<div class="alert alert-primary mb-2" role="alert"><strong>No Reports Found!</strong> You have not filed any reports. File one through the ACARS software or manual report submission to see its details and status on this page.</div>';
                                } else {
                            ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Flight #</th>
                                <th>Departure</th>
                                <th>Arrival</th>
                                <th>Aircraft</th>
                                <th>Flight Time</th>
                                <th>Submitted</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pireps as $report) { ?>
                            <tr>
                                <td><a
                                        href="<?php echo url('/pireps/view/'.$report->pirepid);?>"><?php echo $report->code . $report->flightnum; ?></a>
                                </td>
                                <td><?php echo $report->depicao; ?></td>
                                <td><?php echo $report->arricao; ?></td>
                                <td><?php echo $report->aircraft . " ($report->registration)"; ?></td>
                                <td><?php echo $report->flighttime; ?></td>
                                <td><?php echo date("d/M/Y", $report->submitdate); ?></td>
                                <td>
                                    <?php
                                                if($report->accepted == PIREP_ACCEPTED)
                                                    echo '<div id="success" class="badge badge-success">Accepted</div>';
                                                elseif($report->accepted == PIREP_REJECTED)
                                                    echo '<div id="error" class="badge badge-danger">Rejected</div>';
                                                elseif($report->accepted == PIREP_PENDING)
                                                    echo '<div id="error" class="badge badge-info">Approval Pending</div>';
                                                elseif($report->accepted == PIREP_INPROGRESS)
                                                    echo '<div id="error" class="badge badge-warning">Flight in Progress</div>';
                                                ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <?php } ?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Mail Modal -->
<div class="modal fade" id="composeForm" aria-modal="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">New Message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo url('/Mail');?>" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="text-muted">Recipient</label>
                            <select name="who_to" class="form-control">
                                <option value="">Select a pilot</option>
                                <?php 
											if(PilotGroups::group_has_perm(Auth::$usergroups, ACCESS_ADMIN)) { ?>
                                <option value="all">NOTAM (All Pilots)</option>
                                <?php 
                                            } $allpilots = PilotData::findPilots(array('p.retired' => '0')); foreach($allpilots as $pilots) {
                                                echo '<option value="'.$pilots->pilotid.'">'.$pilots->firstname.' '.$pilots->lastname.' - '.PilotData::GetPilotCode($pilots->code, $pilots->pilotid).'</option>';
                                            }
                                        ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="text-muted">Subject</label>
                            <input type="text" class="form-control" placeholder="Subject" name="subject">
                        </div>

                        <div class="form-group">
                            <label class="text-muted">Message</label>
                            <textarea name="message" class="summernote"></textarea>
                        </div>
                    </div>

                    <div class="modal-footer bg-whitesmoke">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        <input type="hidden" name="who_from" value="<?php echo Auth::$userinfo->pilotid; ?>" />
                        <input type="hidden" name="action" value="send" />
                        <input type="submit" class="btn btn-primary" value="Send">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>