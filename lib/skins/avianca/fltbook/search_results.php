<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>
<?php
	$pilotid = Auth::$userinfo->pilotid;
	$last_location 	= FltbookData::getLocation($pilotid);
	$last_name = OperationsData::getAirportInfo($last_location->arricao);
?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Flight Booking</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv OPS&trade;</a></li>
                    <li class="breadcrumb-item">Flight Operations</li>
                    <li class="breadcrumb-item">Schedule Results</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="row">
                <?php
                if(!$allroutes) {
                    echo '<span class="alert alert-danger">No flights found!</span>';
                } else {
                ?>
                <?php
                foreach($allroutes as $route) {
                    if($settings['disabled_ac_sched_show'] == 0) {
                        # Disable 'fake' aircraft to get hide a lot of schedules at once
                        $aircraft = FltbookData::getAircraftByID($route->aircraftid);
                        if($aircraft->enabled != 1) {
                            continue;
                        }
                    }

                    if(Config::Get('RESTRICT_AIRCRAFT_RANKS') == 1 && Auth::LoggedIn()) {
                        if($route->aircraftlevel > Auth::$userinfo->ranklevel) {
                            continue;
                        }
                    }
            ?>
                <div class="col-md-4">
                    <!-- Widget: user widget style 1 -->
                    <div class="card card-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header text-white"
                            style="background: url('<?php echo SITE_URL;?>/lib/skins/avianca/assets/img/airports/<?php echo $route->arricao ;?>.jpg') center center;">
                            <h3 class="widget-user-username text-right">
                                <b><?php echo $route->code . $route->flightnum?></b>
                            </h3>
                            <h5 class="widget-user-desc text-right">Operated by <?php echo $route->code;?><br>
                                <?php
				$dist = $route->distance;
				$speed = 500;
				$app = $speed / 60;
				$flttime = round($dist / $app, 0) + 20;
				$hours = intval($flttime / 60);
                $minutes = (($flttime / 60) - $hours) * 60;
                 if ($route->flighttime < 2){$resultado = "<span class='badge badge-success'>Short Haul</span>";}else if($route->flighttime < 7){$resultado = "<span class='badge badge-info'>Medium Haul</span>";}else if($route->flighttime < 10){$resultado = "<span class='badge badge-warning'>Long Haul</span>";}else {$resultado = "<span class='badhe badhe-danger'>ULTRA LONG HAUL</span>";}?><small><?php echo $resultado;?></small>
                            </h5>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" src="<?php echo SITE_URL; ?>/lib/skins/avianca/assets/img/logo.svg"
                                alt="User Avatar">
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header mb-2">Departure</h5>
                                        <a href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $route->depicao ;?>"
                                            data-toggle="tooltip" data-placement="bottom" title="More information"
                                            class="description-text btn btn-outline-primary"><?php echo $route->depicao ;?></a>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">Aircraft</h5>
                                        <span class="description-text"><?php echo $route->aircraft ;?>
                                            <br>(<?php echo $route->registration ;?>)</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header mb-2">Arrival</h5>
                                        <a href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $route->arricao ;?>"
                                            data-toggle="tooltip" data-placement="bottom" title="More information"
                                            class="description-text btn btn-outline-primary"><?php echo $route->arricao ;?></a>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="card-body">
                            <div class="row">
                            <div class="col-sm-12 d-flex flex-column">
                                    <img alt="Barcoded value 1234567890"
                                        src="http://bwipjs-api.metafloor.com/?bcid=qrcode&text=<?php echo SITE_URL;?>/index.php/schedules/details/<?php echo $route->id;?>"
                                        class="img-fluid mx-auto">
                                    <p class="text-muted small text-center mt-1">Scan the code to see the flight
                                        briefing
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <?php
						 $aircraft = OperationsData::getAircraftInfo($route->aircraftid);
						 $acbidded = FltbookData::getBidByAircraft($aircraft->id);
						 $check    = SchedulesData::getBidWithRoute(Auth::$userinfo->pilotid, $route->code, $route->flightnum);

                        if($check) {?>
                                <div class="col-sm-4">
                                    <span class="badge badge-danger"> This Flight is Booked!</span>
                                </div>
                                <?php } else {?>
                                <div class="col-sm-6">
                                    <a data-toggle="modal"
                                        href="<?php echo SITE_URL; ?>/action.php/fltbook/confirm?id=<?php echo $route->id; ?>&airline=<?php echo $route->code; ?>&aicao=<?php echo $route->aircrafticao; ?>"
                                        data-target="#confirm" data-toggle="tooltip" data-placement="top"
                                        title="Add bid!" class="btn btn-lg btn-outline-primary btn-block"><i
                                            class="fa fa-plus"></i></a>
                                </div>
                                <?php }
						 ?>
                                <div class="col-sm-6 d-flex flex-column">
                                    <a href="<?php echo SITE_URL;?>/index.php/schedules/details/<?php echo $route->id;?>"
                                        target="_blank" data-toggle="tooltip" data-placement="right"
                                        title="See Flight Info" class="btn btn-lg btn-outline-info btn-block"><i
                                            class="fas fa-info"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.widget-user -->
                </div>
                <?php
/* END OF EACH FLIGHT */
}
}
?>
            </div>
        </div>
    </div>
</div>
<!-- Latest compiled and minified JavaScript - Modified to clear modal on data-dismiss -->
<script type="text/javascript" src="<?php echo SITE_URL; ?>/lib/js/bootstrap.js"></script>