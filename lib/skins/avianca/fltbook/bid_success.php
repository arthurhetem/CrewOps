<script>
    $(window).load(function() {
        Swal.fire({
            title: 'Success!', 
            html: "<?php echo $message; ?>", 
            icon: "success"
        }).then(function() {
        });
    })

    window.setTimeout(function(){
        // Redirect to schedules
        window.location.href = "<?php echo SITE_URL;?>/schedules/bids";
    }, 2000);
</script>