<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Weather Briefing</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv Ops&trade;</a></li>
                    <li class="breadcrumb-item active">Weather</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">METAR</h5>
          </div>
          <!-- /.card-header -->
          <form action="<?php echo url('/wthr/metar');?>" method="post" class="form-horizontal">
            <div class="card-body">
              <div class="form-group row">
                <label for="icao" class="col-sm-2 col-form-label">Insert ICAO</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="icao" name="icao" placeholder="XXXX" required="required" onchange="javascript:this.value=this.value.toUpperCase();">
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            <input type="submit" name="myform" value="Get Current WX" class="btn btn-rounded btn-primary float-right">
            </div>
            <!-- /.card-footer -->
          </form>
          <!-- /form -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            Current Location Weather
          </div>
          <!-- /.card-header -->
         <div class="card-body">
          <?php
            $localAtual = FltbookData::getLocation(Auth::$userinfo->pilotid)->arricao;
            $metar = $_POST['metar'];
            $url = 'https://metar.vatsim.net/'.$localAtual.'';
            $page = file_get_contents($url);

          if(!$localAtual) {
                        echo "<div class='alert alert-primary'><h5><i class='far fa-lightbulb'></i> Oops!</h5> You're at home! We only know the weather on airports!</div>";
                    } else { 
                ?>
                <div class="col-md-9" style="float: none; margin: auto;">
                    <div style="text-align: center; border-radius: 18px; padding: 5px; margin-bottom: 1.5rem; font-weight: 620;" class="alert alert-primary">
                        <?php echo $page; ?>
                    </div>
                </div>
                    <?php } ?>
         </div>
         <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h5 class="cart-title">Weather Map</h5>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
          <iframe width="100%" height="670px" src="https://embed.windy.com/embed2.html?lat=-16.552&lon=-51.855&zoom=4&level=surface&overlay=wind&menu=&message=true&marker=&calendar=&pressure=&type=map&location=coordinates&detail=&detailLat=-23.630&detailLon=-46.632&metricWind=kt&metricTemp=%C2%B0C&radarRange=-1" frameborder="0"></iframe>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#ops').addClass('active');
    $('#weather').addClass('active');
</script>