<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Weather Briefing Package for <?php echo $icao; ?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv Ops&trade;</a></li>
                    <li class="breadcrumb-item active">Weather</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">METAR</h5>
          </div>
          <!-- /.card-header -->
          <form action="<?php echo url('/wthr/metar');?>" method="post" class="form-horizontal">
            <div class="card-body">
              <div class="form-group row">
                <label for="icao" class="col-sm-2 col-form-label">Insert ICAO</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" id="icao" name="icao" placeholder="XXXX" required="required" style="text-transform: uppercase;">
                </div>
              </div>
              <!-- /.form-group -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
            <input type="submit" name="myform" value="Get Current WX" class="btn btn-rounded btn-primary float-right">
            </div>
            <!-- /.card-footer -->
          </form>
          <!-- /form -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            Weather Data
          </div>
          <!-- /.card-header -->
         <div class="card-body">
         <?php echo $metar; ?>
                  <br>
                  <br>
                  <?php if (!$taf){
                    echo "<h3>Indisponible TAF</h3>";
                  }
                  else{
                     echo $taf;
                  }
                  ?>
         </div>
         <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            <h5 class="card-title">METAR</h5>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
              <div class="table-responsive">
              <table class="table table-hover">
              				<tbody><tr>
              					<td><span class="label label-default">Station ID:</span></td>
              					<td><?php echo $stationid;?> (<?php echo $stationname;?>/<?php echo $stationcountry ;?>)</td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Observation Time:</span></td>
              					<td><?php echo $time;?></td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Wind Direction:</span></td>
              					<td><?php echo $winddir;?> &deg; / <?php echo $windspd;?> kts</td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Sky Condition:</span></td>
                        <td><?php
                            if($skycondition0 == "CAVOK"){
                              echo 'CAVOK';
                            }
              							 else if(!$skycondition3 OR !$skycondition4)
              							{
              								echo $skycondition0.' Clouds At '.$skycondition1.' Feet';
              							}
              							else
              							{
              								echo $skycondition0.' Clouds At '.$skycondition1.' Feet / '.$skycondition3.' Clouds At '.$skycondition4.' Feet';
              							}
              						?></td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Visibility:</span></td>
              					<td><?php echo $visibility.$sky1;?> Miles</td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Temperature:</span></td>
              					<td><?php echo $temperature.$sky1;?>° C</td>
              				</tr>
                      <tr>
              					<td><span class="label label-default">Altimeter:</span></td>
              					<td><?php echo $altimeter;?> HPA</td>
              				</tr>
              				</tbody></table>
              </div>
              <!-- /.table-responsive -->
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
      <div class="col-6">
        <div class="card">
          <div class="card-header">
            Airport Operational Condition
          </div>
          <!-- /.card-header -->
         <div class="card-body">
         <?php
                  switch ($flightrules){
    case 'LIFR':
      echo '<h3><span class="badge badge-danger">CLOSED</span></h3> <br> Operations suspended due to extreme weather conditions';
      break;
    case 'IFR':
      echo '<h3><span class="badge bg-indigo">OPEN</span></h3> <br> V F R operations are suspended due to low visibility, the airport is operating only I F R';
      break;
    case 'MVFR':
      echo '<h3><span class="badge badge-warning">OPEN</span></h3> <br> Marginal V F R Operations, request Special V F R and I F R';
      break;
    case 'VFR':
      echo '<h3><span class="badge badge-success">OPEN</span></h3> <br> V F R and I F R operations';
      break;
    default:
      echo '<h3>Insert a <span class="badge badge-default">ICAO</span></h3> <br>No ICAO inserted';
      break;
  } ?>
         </div>
         <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-6 -->
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h5 class="cart-title">Weather Map</h5>
          </div>
          <!-- /.card-header -->
          <div class="card-body p-0">
            <iframe width="100%" height="670px" src="https://embed.windy.com/embed2.html?lat=<?php echo $lat; ?>&lon=<?php echo $lng; ?>&zoom=6&level=surface&overlay=wind&menu=&message=true&marker=&calendar=&pressure=&type=map&location=coordinates&detail=&detailLat=-23.630&detailLon=-46.632&metricWind=kt&metricTemp=%C2%B0C&radarRange=-1" frameborder="0"></iframe>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col-12 -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#ops').addClass('active');
    $('#weather').addClass('active');
</script>