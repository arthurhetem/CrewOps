<?php if(!defined('IN_PHPVMS') && IN_PHPVMS !== true) { die(); } ?>

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Post Flight Report</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv OPS&trade;</a></li>
                    <li class="breadcrumb-item">Flights</li>
                    <li class="breadcrumb-item active"><?php echo $pirep->code . $pirep->flightnum; ?></li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="<?php echo PilotData::getPilotAvatar($pirep->pilotid); ?>"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center"><?php echo $pirep->firstname .' '. $pirep->lastname; ?>
                        </h3>

                        <p class="text-muted text-center">Pilot in Command</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Departure</b> <a class="float-right btn btn-outline-primary"
                                    href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $pirep->depicao; ?>"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="<?php echo $pirep->depicao; ?> - More informations"><?php echo $pirep->depname; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Arrival</b> <a class="float-right btn btn-outline-primary"
                                    href="<?php echo SITE_URL; ?>/airports/get_airport?icao=<?php echo $pirep->arricao; ?>"
                                    data-toggle="tooltip" data-placement="bottom"
                                    title="<?php echo $pirep->arricao; ?> - More informations"><?php echo $pirep->arrname; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Duration</b> <a class="float-right"><?php echo $pirep->flighttime_stamp; ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Distance</b> <a class="float-right"><?php echo $pirep->distance; ?> NM</a>
                            </li>
                            <?php if ($pirep->flighttype == "C"){?>
                            <li class="list-group-item">
                                <b>Cargo</b> <a class="float-right"><?php echo $pirep->load; ?> KGS</a>
                            </li>
                            <?php }else {?>
                            <li class="list-group-item">
                                <b>Passengers</b> <a class="float-right"><?php echo $pirep->load; ?></a>
                            </li>
                            <?php }?>
                            <li class="list-group-item">
                                <b>Fuel</b> <a class="float-right"><?php echo $pirep->fuelused; ?> KGS</a>
                            </li>
                            <li class="list-group-item">
                                <b>Aircraft</b> <a
                                    href="<?php echo SITE_URL; ?>/fleet/view/<?php echo $pirep->aircraftid; ?>"
                                    data-toggle="tooltip" data-placement="bottom" title="More information"
                                    class="float-right btn btn-outline-primary"><?php echo $pirep->aircraft." ($pirep->registration)"?></a>
                            </li>
                        </ul>
                        <div class="h2 text-center">
                            <?php
									if($pirep->accepted == PIREP_ACCEPTED){
			echo '<span class="badge badge-success">Approved</span>';
		}elseif($pirep->accepted == PIREP_REJECTED){
			echo '<span class="badge badge-danger">Rejected</span>';
		}elseif($pirep->accepted == PIREP_PENDING){
			echo '<span class="badge badge-warning">Pending</span>';
		}elseif($pirep->accepted == PIREP_INPROGRESS){
			echo '<span class="badge badge-info">In Progress</span>';

									}
									?>
                            </h2>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="box-footer bg-indigo">
                        <h1 class="text-center"><?php echo $pirep->source; ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header card-success card-outline">
                        <h3 class="card-title">PIREP Evaluation and RAW Report</h3>
                    </div>
                    <div class="card-body">
                        <?php
				//EFICIENCIA
					$comb = $pirep->fuelprice;
					$lucro = $pirep->revenue;
					$p20 = (20/100) * $pirep->revenue;
					$p30 = (30/100) * $pirep->revenue;
					$p40 = (40/100) * $pirep->revenue;
					if ($comb < $p20){
						$calculou = 80;
					}elseif ($comb < $p30){
						$calculou = 70;
					}elseif ($comb < $p40){
						$calculou = 60;
					}
					// LUCRO
					$despesa = $pirep->expenses;
					$soma = $lucro + $despesa;
					$pct = ($soma / $lucro) * 100;
					$pct = round($pct);
					// POUSO
					$ldg = $pirep->landingrate;
					$max = -650;
					$pctPouso = (($max - $ldg) /$max * 100);
					$pctPouso = round($pctPouso);
                 ?>
                        <div class="row justify-content-center">
                            <div class="col-md-3">
                                <div class="c100 p<?php echo $calculou; ?> green">
                                    <span><?php echo $calculou; ?>% <i class="fas fa-leaf"></i></span>
                                    <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="c100 p<?php echo $pct; ?> blue">
                                    <span><?php echo $pct; ?> % $</span>
                                    <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="c100 p<?php echo $pctPouso; ?> green">
                                    <span><?php echo $pctPouso; ?> % <i class="fas fa-plane-arrival"></i></span>
                                    <div class="slice">
                                        <div class="bar"></div>
                                        <div class="fill"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <blockquote class="quote-<?php 
                        if($pirep->accepted == PIREP_ACCEPTED){ 
                            echo 'success';
                        }elseif($pirep->accepted == PIREP_REJECTED){
                            echo 'danger';
                        }elseif($pirep->accepted == PIREP_PENDING){
                            echo 'warning';
                        }elseif($pirep->accepted == PIREP_INPROGRESS){
                            echo 'primary';
                        }?> " style="overflow-y: scroll; height: 455px;"><?php
										# Simple, each line of the log ends with *
										# Just explode and loop.
										$log = explode('*', $pirep->log);
										foreach($log as $line)
										{
											echo $line .'<br />';
										}
										?></blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php if ($pirep->pilotid != Auth::$pilot->pilotid) { } else {?>
            <div class="col-md-6">
                <div class="card direct-chat direct-chat-primary card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Comments</h3>
                    </div>
                    <div class="card-body">
                        <div class="direct-chat-messages">
                            <?php 
                        $comments = PIREPData::GetComments($pirep->id);
                        if(!$comments) {
                            echo '<div class="badge badge-info col-md-12">There are no comments for this PIREP.</div>';
                        } else {
                            foreach($comments as $comment) {
                    ?>
                            <div
                                class="direct-chat-msg <?php if ($comment->pilotid == Auth::$pilot->pilotid) { echo 'right';}?>">
                                <div class="direct-chat-infos clearfix">
                                    <span class="direct-chat-name float-left">
                                        <a
                                            href="<?php echo SITE_URL;?>/profile/view/<?php echo $comment->pilotid;?>"><?php echo $comment->firstname.' '.$comment->lastname; ?></a>
                                    </span>
                                    <span
                                        class="direct-chat-timestamp float-right"><?php echo date("d/M/Y, H:i", $comment->postdate); ?></span>
                                </div>
                                <!-- /.direct-chat-infos -->
                                <img class="direct-chat-img img-bordered-sm"
                                    src="<?php echo PilotData::getPilotAvatar($comment->pilotid); ?>" alt="user image">
                                <div class="direct-chat-text">
                                    <?php echo $comment->comment;?>
                                </div>
                            </div>
                            <?php } } ?>
                        </div>
                    </div>
                    <div class="card-footer" style="display: block;">
                        <form action="<?php echo url('/pireps/viewpireps');?>" method="post">
                            <div class="input-group">
                                <input type="text" name="comment" placeholder="Type comment ..." class="form-control">
                                <input type="hidden" name="action" value="addcomment" />
                                <input type="hidden" name="pirepid" value="<?php echo $pirep->pirepid?>" />
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-primary">Add comment <i
                                            class="far fa-paper-plane"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card bg-gradient-dark">
                <div class="card-header">
                    <h3 class="card-title">PIREP Route</h3>
                </div>
                <div class="card-body">
                    <blockquote class="bg-dark">
                        <?php 
                            if(!$pirep->route) {
                                echo 'This pirep don\'t have a route';
                            } else {
                                echo $pirep->route;
                            }  
                        ?>
                    </blockquote>
                    <script src="https://rawgit.com/mapshakers/leaflet-icon-pulse/master/src/L.Icon.Pulse.js"></script>
                    <?php require 'route_map.php'; ?>
                </div>
            </div>
        </div>
    </div>
</div>