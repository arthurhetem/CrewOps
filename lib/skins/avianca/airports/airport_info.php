<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?php echo $name->icao; ?> Airport</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript::">ONEv Info&trade;</a></li>
                    <li class="breadcrumb-item active"><?php echo $name->icao; ?> Airport</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="col-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title"><?php echo $name->name; ?></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <div class="table-responsive">
                    <table class="table mb-0">
                    <tr>
                        <th>ICAO:</th>
                        <td><?php echo $name->icao; ?></td>
                        <th>Country:</th>
                        <td><?php echo $name->country; ?></td>
                    </tr>

                    <tr>
                        <th>Latitude:</th>
                        <td><?php echo $name->lat; ?></td>
                        <th>Longitude:</th>
                        <td><?php echo $name->lng; ?></td>
                    </tr>

                    <tr>
                        <th>Total Arrivals:</th>
                        <td><?php echo AirportData::getarrflights($name->icao); ?></td>

                        <th>Total Departures:</th>
                        <td><?php echo AirportData::getdeptflights($name->icao); ?></td>
                    </tr>

                    <?php 
                        $icao = $name->icao;
                        $params = array(
                            'depicao'   => $icao,
                            'accepted'  => '1'
                            );
                        $pireps = PIREPData::findPIREPS($params, 1);
                        $deppirep = $pireps[0];
                        $params = array(
                            'arricao'   => $icao,
                            'accepted'  => '1'
                            );
                        $pireps = PIREPData::findPIREPS($params, 1);
                        $arrpirep = $pireps[0];

                        $initialdep = substr($deppirep->firstname,0,1);
                        $initialarr = substr($arrpirep->firstname,0,1);
                    ?>
                    <tr>
                        <th>Latest Arrival:</th>
                        <td><a href="<?php echo SITE_URL?>/index.php/pireps/viewreport/<?php echo $arrpirep->pirepid;?>"><?php echo $arrpirep->code.$arrpirep->flightnum.' ('.$arrpirep->depicao.'-'.$arrpirep->arricao.')</a> - '.$arrpirep->firstname.' '.$arrpirep->lastname?></td>
                        <th>Latest Departure:</th>
                        <td><a href="<?php echo SITE_URL?>/index.php/pireps/viewreport/<?php echo $deppirep->pirepid;?>"><?php echo $deppirep->code.$deppirep->flightnum.' ('.$deppirep->depicao.'-'.$deppirep->arricao.')</a> - '.$deppirep->firstname.' '.$deppirep->lastname?></td>
                    </tr>
                    <tr>
                        <th>METAR:</th>
                        <td>
                            <?php
                            $metar = $_POST['metar'];
                            $url = 'http://metar.vatsim.net/'.$name->icao.'';
                            $page = file_get_contents($url);
                            echo $page;
                            ?>
                        </td>
                        <th></th>
                        <td></td>
                    </tr>
                    
				</table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card">
                <div class="card-body p-0">
                    <div id="airportmap" style="border-radius: 9px; width: 100%; height: 540px; position: relative; overflow: hidden;"></div>
			</div>
		</div>
        </div>
        <!-- /.col-12 -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
<script>
    $('#info').addClass('active');
    $('#airports').addClass('active');
</script>
<script src="<?php echo SITE_URL?>/lib/js/base_map.js"></script>
<script type="text/javascript">
	const map = createMap ({
		render_elem: 'airportmap',
		provider: '<?php echo Config::Get("MAP_TYPE"); ?>',
		zoom: 14,
		center: L.latLng("<?php echo $name->lat; ?>", "<?php echo $name->lng; ?>")
	});

	L.marker(["<?php echo $name->lat; ?>", "<?php echo $name->lng; ?>"]).addTo(map)
</script>